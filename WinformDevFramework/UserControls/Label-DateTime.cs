﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformDevFramework.UserControls
{
    public partial class Label_DateTime : UserControl
    {
        public DateTimePicker DateTimePicker
        {
            get
            {
                return this.dateTimePicker;
            }
        }

        public Label Label
        {
            get
            {
                return this.label;
            }
        }
        public Label_DateTime()
        {
            InitializeComponent();
        }
    }
}
