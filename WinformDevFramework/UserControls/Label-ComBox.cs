﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinformDevFramework.UserControls
{
    public partial class Label_ComBox : UserControl
    {
        public ComboBox ComboBox
        {
            get
            {
                return this.comboBox;
            }
        }

        public Label Label
        {
            get
            {
                return this.label;
            }
        }
        public Label_ComBox()
        {
            InitializeComponent();
        }
    }
}
