namespace WinformDevFramework
{
    partial class Frmw_SaleReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSaleReturnCode = new System.Windows.Forms.Label();
            this.SaleReturnCode = new System.Windows.Forms.TextBox();
            this.lblCustomerCode = new System.Windows.Forms.Label();
            this.CustomerCode = new System.Windows.Forms.TextBox();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.CustomerName = new System.Windows.Forms.TextBox();
            this.lblInvoicesDate = new System.Windows.Forms.Label();
            this.InvoicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblMakeUserID = new System.Windows.Forms.Label();
            this.lblInvoicesFile = new System.Windows.Forms.Label();
            this.InvoicesFile = new System.Windows.Forms.TextBox();
            this.lblSaleCode = new System.Windows.Forms.Label();
            this.SaleCode = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.TextBox();
            this.lblReviewUserID = new System.Windows.Forms.Label();
            this.ReviewUserID = new System.Windows.Forms.NumericUpDown();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.GoodsName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPriceD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalTaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleReturnCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleReturnDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPrice2 = new System.Windows.Forms.TextBox();
            this.lblTotalPrice2 = new System.Windows.Forms.Label();
            this.TotalPrice1 = new System.Windows.Forms.TextBox();
            this.lblTotalPrice1 = new System.Windows.Forms.Label();
            this.TotalPrice = new System.Windows.Forms.TextBox();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.MakeUser = new System.Windows.Forms.ComboBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReviewUserID)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Size = new System.Drawing.Size(423, 214);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MakeUser);
            this.groupBox1.Controls.Add(this.TotalPrice2);
            this.groupBox1.Controls.Add(this.lblTotalPrice2);
            this.groupBox1.Controls.Add(this.TotalPrice1);
            this.groupBox1.Controls.Add(this.lblTotalPrice1);
            this.groupBox1.Controls.Add(this.TotalPrice);
            this.groupBox1.Controls.Add(this.lblTotalPrice);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.ReviewUserID);
            this.groupBox1.Controls.Add(this.lblReviewUserID);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.SaleCode);
            this.groupBox1.Controls.Add(this.lblSaleCode);
            this.groupBox1.Controls.Add(this.InvoicesFile);
            this.groupBox1.Controls.Add(this.lblInvoicesFile);
            this.groupBox1.Controls.Add(this.lblMakeUserID);
            this.groupBox1.Controls.Add(this.InvoicesDate);
            this.groupBox1.Controls.Add(this.lblInvoicesDate);
            this.groupBox1.Controls.Add(this.CustomerName);
            this.groupBox1.Controls.Add(this.lblCustomerName);
            this.groupBox1.Controls.Add(this.CustomerCode);
            this.groupBox1.Controls.Add(this.lblCustomerCode);
            this.groupBox1.Controls.Add(this.SaleReturnCode);
            this.groupBox1.Controls.Add(this.lblSaleReturnCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(411, 204);
            this.groupBox1.Controls.SetChildIndex(this.lblSaleReturnCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SaleReturnCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblCustomerCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblCustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesFile, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesFile, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSaleCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SaleCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.Status, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblReviewUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.ReviewUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice1, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice1, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice2, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice2, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUser, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(689, 36);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAdd.Size = new System.Drawing.Size(53, 25);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(59, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEdit.Size = new System.Drawing.Size(53, 25);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(116, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSave.Size = new System.Drawing.Size(53, 25);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(173, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCanel.Size = new System.Drawing.Size(53, 25);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(230, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDel.Size = new System.Drawing.Size(53, 25);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(287, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnResetPW.Size = new System.Drawing.Size(53, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(401, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClose.Size = new System.Drawing.Size(53, 25);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(344, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSearch.Size = new System.Drawing.Size(53, 25);
            // 
            // lblSaleReturnCode
            // 
            this.lblSaleReturnCode.Location = new System.Drawing.Point(10, 10);
            this.lblSaleReturnCode.Name = "lblSaleReturnCode";
            this.lblSaleReturnCode.Size = new System.Drawing.Size(85, 23);
            this.lblSaleReturnCode.TabIndex = 2;
            this.lblSaleReturnCode.Text = "退货单号";
            this.lblSaleReturnCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SaleReturnCode
            // 
            this.SaleReturnCode.Location = new System.Drawing.Point(100, 10);
            this.SaleReturnCode.Name = "SaleReturnCode";
            this.SaleReturnCode.Size = new System.Drawing.Size(130, 23);
            this.SaleReturnCode.TabIndex = 1;
            // 
            // lblCustomerCode
            // 
            this.lblCustomerCode.Location = new System.Drawing.Point(10, 45);
            this.lblCustomerCode.Name = "lblCustomerCode";
            this.lblCustomerCode.Size = new System.Drawing.Size(85, 23);
            this.lblCustomerCode.TabIndex = 4;
            this.lblCustomerCode.Text = "客户编码";
            this.lblCustomerCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomerCode
            // 
            this.CustomerCode.Location = new System.Drawing.Point(100, 45);
            this.CustomerCode.Name = "CustomerCode";
            this.CustomerCode.Size = new System.Drawing.Size(130, 23);
            this.CustomerCode.TabIndex = 3;
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.Location = new System.Drawing.Point(230, 45);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(85, 23);
            this.lblCustomerName.TabIndex = 6;
            this.lblCustomerName.Text = "客户名称";
            this.lblCustomerName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomerName
            // 
            this.CustomerName.Location = new System.Drawing.Point(320, 45);
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.Size = new System.Drawing.Size(130, 23);
            this.CustomerName.TabIndex = 5;
            // 
            // lblInvoicesDate
            // 
            this.lblInvoicesDate.Location = new System.Drawing.Point(450, 10);
            this.lblInvoicesDate.Name = "lblInvoicesDate";
            this.lblInvoicesDate.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesDate.TabIndex = 8;
            this.lblInvoicesDate.Text = "单据日期";
            this.lblInvoicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvoicesDate
            // 
            this.InvoicesDate.Location = new System.Drawing.Point(540, 10);
            this.InvoicesDate.Name = "InvoicesDate";
            this.InvoicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvoicesDate.TabIndex = 7;
            // 
            // lblMakeUserID
            // 
            this.lblMakeUserID.Location = new System.Drawing.Point(10, 80);
            this.lblMakeUserID.Name = "lblMakeUserID";
            this.lblMakeUserID.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUserID.TabIndex = 10;
            this.lblMakeUserID.Text = "制单人";
            this.lblMakeUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblInvoicesFile
            // 
            this.lblInvoicesFile.Location = new System.Drawing.Point(450, 45);
            this.lblInvoicesFile.Name = "lblInvoicesFile";
            this.lblInvoicesFile.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesFile.TabIndex = 12;
            this.lblInvoicesFile.Text = "单据附件";
            this.lblInvoicesFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblInvoicesFile.Visible = false;
            // 
            // InvoicesFile
            // 
            this.InvoicesFile.Location = new System.Drawing.Point(540, 45);
            this.InvoicesFile.Name = "InvoicesFile";
            this.InvoicesFile.Size = new System.Drawing.Size(130, 23);
            this.InvoicesFile.TabIndex = 11;
            this.InvoicesFile.Visible = false;
            // 
            // lblSaleCode
            // 
            this.lblSaleCode.Location = new System.Drawing.Point(230, 10);
            this.lblSaleCode.Name = "lblSaleCode";
            this.lblSaleCode.Size = new System.Drawing.Size(85, 23);
            this.lblSaleCode.TabIndex = 14;
            this.lblSaleCode.Text = "销售单号";
            this.lblSaleCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SaleCode
            // 
            this.SaleCode.Location = new System.Drawing.Point(320, 10);
            this.SaleCode.Name = "SaleCode";
            this.SaleCode.Size = new System.Drawing.Size(130, 23);
            this.SaleCode.TabIndex = 13;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 118);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 16;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 118);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 15;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(450, 80);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(85, 23);
            this.lblStatus.TabIndex = 18;
            this.lblStatus.Text = "单据状态";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblStatus.Visible = false;
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(540, 80);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(130, 23);
            this.Status.TabIndex = 17;
            this.Status.Visible = false;
            // 
            // lblReviewUserID
            // 
            this.lblReviewUserID.Location = new System.Drawing.Point(230, 80);
            this.lblReviewUserID.Name = "lblReviewUserID";
            this.lblReviewUserID.Size = new System.Drawing.Size(85, 23);
            this.lblReviewUserID.TabIndex = 20;
            this.lblReviewUserID.Text = "审核人";
            this.lblReviewUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblReviewUserID.Visible = false;
            // 
            // ReviewUserID
            // 
            this.ReviewUserID.Location = new System.Drawing.Point(320, 80);
            this.ReviewUserID.Name = "ReviewUserID";
            this.ReviewUserID.Size = new System.Drawing.Size(130, 23);
            this.ReviewUserID.TabIndex = 19;
            this.ReviewUserID.Visible = false;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(2, 189);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(404, 0);
            this.tabControl2.TabIndex = 24;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(396, 0);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "退货明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AllowUserToOrderColumns = true;
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeight = 40;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GoodsName,
            this.GoodsCode,
            this.GoodsSpec,
            this.GoodsUnit,
            this.Number,
            this.UnitPrice,
            this.TotalPriceD,
            this.TaxRate,
            this.DiscountPrice,
            this.TaxUnitPrice,
            this.TaxPrice,
            this.TotalTaxPrice,
            this.RemarkD,
            this.SaleCodeD,
            this.SaleReturnCodeD,
            this.SaleReturnDetailCode});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.EnableHeadersVisualStyles = false;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowHeadersWidth = 72;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(390, 0);
            this.dataGridViewDetail.TabIndex = 0;
            // 
            // GoodsName
            // 
            this.GoodsName.HeaderText = "商品";
            this.GoodsName.MinimumWidth = 9;
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.Width = 39;
            // 
            // GoodsCode
            // 
            this.GoodsCode.DataPropertyName = "GoodsCode";
            this.GoodsCode.HeaderText = "商品编码";
            this.GoodsCode.MinimumWidth = 9;
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.ReadOnly = true;
            this.GoodsCode.Width = 64;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.DataPropertyName = "GoodsSpec";
            this.GoodsSpec.HeaderText = "商品规格";
            this.GoodsSpec.MinimumWidth = 9;
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.ReadOnly = true;
            this.GoodsSpec.Width = 64;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.DataPropertyName = "GoodsUnit";
            this.GoodsUnit.HeaderText = "计量单位";
            this.GoodsUnit.MinimumWidth = 9;
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.ReadOnly = true;
            this.GoodsUnit.Width = 64;
            // 
            // Number
            // 
            this.Number.DataPropertyName = "Number";
            this.Number.HeaderText = "数量";
            this.Number.MinimumWidth = 9;
            this.Number.Name = "Number";
            this.Number.Width = 53;
            // 
            // UnitPrice
            // 
            this.UnitPrice.DataPropertyName = "UnitPrice";
            this.UnitPrice.HeaderText = "单价";
            this.UnitPrice.MinimumWidth = 9;
            this.UnitPrice.Name = "UnitPrice";
            this.UnitPrice.Width = 53;
            // 
            // TotalPriceD
            // 
            this.TotalPriceD.DataPropertyName = "TotalPrice";
            this.TotalPriceD.HeaderText = "金额";
            this.TotalPriceD.MinimumWidth = 9;
            this.TotalPriceD.Name = "TotalPriceD";
            this.TotalPriceD.ReadOnly = true;
            this.TotalPriceD.Width = 53;
            // 
            // TaxRate
            // 
            this.TaxRate.DataPropertyName = "TaxRate";
            this.TaxRate.HeaderText = "税率";
            this.TaxRate.MinimumWidth = 9;
            this.TaxRate.Name = "TaxRate";
            this.TaxRate.Width = 53;
            // 
            // DiscountPrice
            // 
            this.DiscountPrice.DataPropertyName = "DiscountPrice";
            this.DiscountPrice.HeaderText = "折扣金额";
            this.DiscountPrice.MinimumWidth = 9;
            this.DiscountPrice.Name = "DiscountPrice";
            this.DiscountPrice.Width = 64;
            // 
            // TaxUnitPrice
            // 
            this.TaxUnitPrice.DataPropertyName = "TaxUnitPrice";
            this.TaxUnitPrice.HeaderText = "含税单价";
            this.TaxUnitPrice.MinimumWidth = 9;
            this.TaxUnitPrice.Name = "TaxUnitPrice";
            this.TaxUnitPrice.ReadOnly = true;
            this.TaxUnitPrice.Width = 64;
            // 
            // TaxPrice
            // 
            this.TaxPrice.DataPropertyName = "TaxPrice";
            this.TaxPrice.HeaderText = "税额";
            this.TaxPrice.MinimumWidth = 9;
            this.TaxPrice.Name = "TaxPrice";
            this.TaxPrice.ReadOnly = true;
            this.TaxPrice.Width = 53;
            // 
            // TotalTaxPrice
            // 
            this.TotalTaxPrice.DataPropertyName = "TotalTaxPrice";
            this.TotalTaxPrice.HeaderText = "含税总价";
            this.TotalTaxPrice.MinimumWidth = 9;
            this.TotalTaxPrice.Name = "TotalTaxPrice";
            this.TotalTaxPrice.ReadOnly = true;
            this.TotalTaxPrice.Width = 64;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 9;
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.Width = 53;
            // 
            // SaleCodeD
            // 
            this.SaleCodeD.DataPropertyName = "SaleCode";
            this.SaleCodeD.HeaderText = "销售单号";
            this.SaleCodeD.MinimumWidth = 9;
            this.SaleCodeD.Name = "SaleCodeD";
            this.SaleCodeD.ReadOnly = true;
            this.SaleCodeD.Width = 64;
            // 
            // SaleReturnCodeD
            // 
            this.SaleReturnCodeD.DataPropertyName = "SaleReturnCode";
            this.SaleReturnCodeD.HeaderText = "退货单号";
            this.SaleReturnCodeD.MinimumWidth = 9;
            this.SaleReturnCodeD.Name = "SaleReturnCodeD";
            this.SaleReturnCodeD.ReadOnly = true;
            this.SaleReturnCodeD.Width = 64;
            // 
            // SaleReturnDetailCode
            // 
            this.SaleReturnDetailCode.DataPropertyName = "SaleReturnDetailCode";
            this.SaleReturnDetailCode.HeaderText = "明细单号";
            this.SaleReturnDetailCode.MinimumWidth = 9;
            this.SaleReturnDetailCode.Name = "SaleReturnDetailCode";
            this.SaleReturnDetailCode.ReadOnly = true;
            this.SaleReturnDetailCode.Width = 64;
            // 
            // TotalPrice2
            // 
            this.TotalPrice2.Location = new System.Drawing.Point(540, 118);
            this.TotalPrice2.Name = "TotalPrice2";
            this.TotalPrice2.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice2.TabIndex = 29;
            this.TotalPrice2.Visible = false;
            // 
            // lblTotalPrice2
            // 
            this.lblTotalPrice2.Location = new System.Drawing.Point(450, 118);
            this.lblTotalPrice2.Name = "lblTotalPrice2";
            this.lblTotalPrice2.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice2.TabIndex = 30;
            this.lblTotalPrice2.Text = "未结金额";
            this.lblTotalPrice2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTotalPrice2.Visible = false;
            // 
            // TotalPrice1
            // 
            this.TotalPrice1.Location = new System.Drawing.Point(320, 118);
            this.TotalPrice1.Name = "TotalPrice1";
            this.TotalPrice1.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice1.TabIndex = 27;
            this.TotalPrice1.Visible = false;
            // 
            // lblTotalPrice1
            // 
            this.lblTotalPrice1.Location = new System.Drawing.Point(230, 118);
            this.lblTotalPrice1.Name = "lblTotalPrice1";
            this.lblTotalPrice1.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice1.TabIndex = 28;
            this.lblTotalPrice1.Text = "已结金额";
            this.lblTotalPrice1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTotalPrice1.Visible = false;
            // 
            // TotalPrice
            // 
            this.TotalPrice.Location = new System.Drawing.Point(320, 80);
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice.TabIndex = 25;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.Location = new System.Drawing.Point(230, 80);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice.TabIndex = 26;
            this.lblTotalPrice.Text = "总金额";
            this.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MakeUser
            // 
            this.MakeUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MakeUser.FormattingEnabled = true;
            this.MakeUser.Location = new System.Drawing.Point(100, 81);
            this.MakeUser.Name = "MakeUser";
            this.MakeUser.Size = new System.Drawing.Size(130, 25);
            this.MakeUser.TabIndex = 31;
            // 
            // Frmw_SaleReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_SaleReturn";
            this.Text = "Frmw_SaleReturn";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReviewUserID)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox SaleReturnCode;
        private Label lblSaleReturnCode;
        private TextBox CustomerCode;
        private Label lblCustomerCode;
        private TextBox CustomerName;
        private Label lblCustomerName;
        private DateTimePicker InvoicesDate;
        private Label lblInvoicesDate;
        private Label lblMakeUserID;
        private TextBox InvoicesFile;
        private Label lblInvoicesFile;
        private TextBox SaleCode;
        private Label lblSaleCode;
        private TextBox Remark;
        private Label lblRemark;
        private TextBox Status;
        private Label lblStatus;
        private NumericUpDown ReviewUserID;
        private Label lblReviewUserID;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private TextBox TotalPrice2;
        private Label lblTotalPrice2;
        private TextBox TotalPrice1;
        private Label lblTotalPrice1;
        private TextBox TotalPrice;
        private Label lblTotalPrice;
        private DataGridViewComboBoxColumn GoodsName;
        private DataGridViewTextBoxColumn GoodsCode;
        private DataGridViewTextBoxColumn GoodsSpec;
        private DataGridViewTextBoxColumn GoodsUnit;
        private DataGridViewTextBoxColumn Number;
        private DataGridViewTextBoxColumn UnitPrice;
        private DataGridViewTextBoxColumn TotalPriceD;
        private DataGridViewTextBoxColumn TaxRate;
        private DataGridViewTextBoxColumn DiscountPrice;
        private DataGridViewTextBoxColumn TaxUnitPrice;
        private DataGridViewTextBoxColumn TaxPrice;
        private DataGridViewTextBoxColumn TotalTaxPrice;
        private DataGridViewTextBoxColumn RemarkD;
        private DataGridViewTextBoxColumn SaleCodeD;
        private DataGridViewTextBoxColumn SaleReturnCodeD;
        private DataGridViewTextBoxColumn SaleReturnDetailCode;
        private ComboBox MakeUser;
    }
}