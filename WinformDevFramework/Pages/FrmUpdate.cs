﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace WinformDevFramework
{
    public partial class FrmUpdate : Form
    {
        public string url = "";
        public string oldVersion = "";
        public string newVsersion = "";
        public FrmUpdate()
        {
            InitializeComponent();
        }

        private void FrmUpdate_Load(object sender, EventArgs e)
        {
            labUpdateVersionTitle.Text = $"WinformDevFramework {newVsersion} 现在可用。您安装的版本是 {oldVersion}。是否更新？";
            this.webView21.ScrollBarsEnabled = false;
            webView21.Url = new Uri(url);
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.DialogResult= DialogResult.Yes;
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.DialogResult=DialogResult.No;
        }

        private void webView21_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //// 注入CSS样式表来隐藏滚动条
            //string css = "::-webkit-scrollbar { display: none; }"; // 隐藏滚动条的CSS样式
            //webView21.Document.InvokeScript("execScript", new object[] { css, "JavaScript" });
        }
    }
}
