namespace WinformDevFramework
{
    partial class Frmw_OtherOutWarehouse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTypeCode = new System.Windows.Forms.Label();
            this.lblInvoicesDate = new System.Windows.Forms.Label();
            this.InvoicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblInvoicesImage = new System.Windows.Forms.Label();
            this.InvoicesImage = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.TextBox();
            this.lblMakeUserID = new System.Windows.Forms.Label();
            this.lblOtherOutWarehouseCode = new System.Windows.Forms.Label();
            this.OtherOutWarehouseCode = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Warehouse = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OtherOutWarehouseCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OtherOutWarehouseDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeCode = new System.Windows.Forms.ComboBox();
            this.MakeUser = new System.Windows.Forms.ComboBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Size = new System.Drawing.Size(423, 214);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MakeUser);
            this.groupBox1.Controls.Add(this.TypeCode);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.OtherOutWarehouseCode);
            this.groupBox1.Controls.Add(this.lblOtherOutWarehouseCode);
            this.groupBox1.Controls.Add(this.lblMakeUserID);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.InvoicesImage);
            this.groupBox1.Controls.Add(this.lblInvoicesImage);
            this.groupBox1.Controls.Add(this.InvoicesDate);
            this.groupBox1.Controls.Add(this.lblInvoicesDate);
            this.groupBox1.Controls.Add(this.lblTypeCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(411, 204);
            this.groupBox1.Controls.SetChildIndex(this.lblTypeCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.Status, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblOtherOutWarehouseCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.OtherOutWarehouseCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.TypeCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUser, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAdd.Size = new System.Drawing.Size(53, 25);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(59, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEdit.Size = new System.Drawing.Size(53, 25);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(116, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSave.Size = new System.Drawing.Size(53, 25);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(173, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCanel.Size = new System.Drawing.Size(53, 25);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(230, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDel.Size = new System.Drawing.Size(53, 25);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(287, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnResetPW.Size = new System.Drawing.Size(53, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(401, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClose.Size = new System.Drawing.Size(53, 25);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(344, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSearch.Size = new System.Drawing.Size(53, 25);
            // 
            // lblTypeCode
            // 
            this.lblTypeCode.Location = new System.Drawing.Point(230, 10);
            this.lblTypeCode.Name = "lblTypeCode";
            this.lblTypeCode.Size = new System.Drawing.Size(85, 23);
            this.lblTypeCode.TabIndex = 2;
            this.lblTypeCode.Text = "业务类别";
            this.lblTypeCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblInvoicesDate
            // 
            this.lblInvoicesDate.Location = new System.Drawing.Point(450, 10);
            this.lblInvoicesDate.Name = "lblInvoicesDate";
            this.lblInvoicesDate.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesDate.TabIndex = 4;
            this.lblInvoicesDate.Text = "单据日期";
            this.lblInvoicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvoicesDate
            // 
            this.InvoicesDate.Location = new System.Drawing.Point(540, 10);
            this.InvoicesDate.Name = "InvoicesDate";
            this.InvoicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvoicesDate.TabIndex = 3;
            // 
            // lblInvoicesImage
            // 
            this.lblInvoicesImage.Location = new System.Drawing.Point(10, 44);
            this.lblInvoicesImage.Name = "lblInvoicesImage";
            this.lblInvoicesImage.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesImage.TabIndex = 6;
            this.lblInvoicesImage.Text = "单据附件";
            this.lblInvoicesImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblInvoicesImage.Visible = false;
            // 
            // InvoicesImage
            // 
            this.InvoicesImage.Location = new System.Drawing.Point(100, 44);
            this.InvoicesImage.Name = "InvoicesImage";
            this.InvoicesImage.Size = new System.Drawing.Size(130, 23);
            this.InvoicesImage.TabIndex = 5;
            this.InvoicesImage.Visible = false;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 79);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 8;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 79);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 7;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(450, 44);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(85, 23);
            this.lblStatus.TabIndex = 10;
            this.lblStatus.Text = "单据状态";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblStatus.Visible = false;
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(540, 44);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(130, 23);
            this.Status.TabIndex = 9;
            this.Status.Visible = false;
            // 
            // lblMakeUserID
            // 
            this.lblMakeUserID.Location = new System.Drawing.Point(10, 44);
            this.lblMakeUserID.Name = "lblMakeUserID";
            this.lblMakeUserID.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUserID.TabIndex = 12;
            this.lblMakeUserID.Text = "制单人";
            this.lblMakeUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblOtherOutWarehouseCode
            // 
            this.lblOtherOutWarehouseCode.Location = new System.Drawing.Point(10, 10);
            this.lblOtherOutWarehouseCode.Name = "lblOtherOutWarehouseCode";
            this.lblOtherOutWarehouseCode.Size = new System.Drawing.Size(85, 23);
            this.lblOtherOutWarehouseCode.TabIndex = 14;
            this.lblOtherOutWarehouseCode.Text = "出库单号";
            this.lblOtherOutWarehouseCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OtherOutWarehouseCode
            // 
            this.OtherOutWarehouseCode.Location = new System.Drawing.Point(100, 10);
            this.OtherOutWarehouseCode.Name = "OtherOutWarehouseCode";
            this.OtherOutWarehouseCode.PlaceholderText = "为空自动生成";
            this.OtherOutWarehouseCode.Size = new System.Drawing.Size(130, 23);
            this.OtherOutWarehouseCode.TabIndex = 13;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(3, 108);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(404, 91);
            this.tabControl2.TabIndex = 20;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(396, 61);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "退货明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Warehouse,
            this.GoodsName,
            this.GoodsCode,
            this.GoodsSpec,
            this.GoodsUnit,
            this.Number,
            this.RemarkD,
            this.OtherOutWarehouseCodeD,
            this.OtherOutWarehouseDetailCode});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowHeadersWidth = 72;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(390, 55);
            this.dataGridViewDetail.TabIndex = 1;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 9;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            this.ID.Width = 27;
            // 
            // Warehouse
            // 
            this.Warehouse.HeaderText = "仓库";
            this.Warehouse.MinimumWidth = 9;
            this.Warehouse.Name = "Warehouse";
            this.Warehouse.Width = 39;
            // 
            // GoodsName
            // 
            this.GoodsName.HeaderText = "商品名称";
            this.GoodsName.MinimumWidth = 9;
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GoodsName.Width = 62;
            // 
            // GoodsCode
            // 
            this.GoodsCode.DataPropertyName = "GoodsCode";
            this.GoodsCode.HeaderText = "商品编码";
            this.GoodsCode.MinimumWidth = 9;
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.ReadOnly = true;
            this.GoodsCode.Width = 81;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.DataPropertyName = "GoodsSpec";
            this.GoodsSpec.HeaderText = "商品规格";
            this.GoodsSpec.MinimumWidth = 9;
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.ReadOnly = true;
            this.GoodsSpec.Width = 81;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.DataPropertyName = "GoodsUnit";
            this.GoodsUnit.HeaderText = "计量单位";
            this.GoodsUnit.MinimumWidth = 9;
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.ReadOnly = true;
            this.GoodsUnit.Width = 81;
            // 
            // Number
            // 
            this.Number.DataPropertyName = "Number";
            this.Number.HeaderText = "数量";
            this.Number.MinimumWidth = 9;
            this.Number.Name = "Number";
            this.Number.Width = 57;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 9;
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.Width = 57;
            // 
            // OtherOutWarehouseCodeD
            // 
            this.OtherOutWarehouseCodeD.DataPropertyName = "OtherOutWarehouseCode";
            this.OtherOutWarehouseCodeD.HeaderText = "出库单号";
            this.OtherOutWarehouseCodeD.MinimumWidth = 9;
            this.OtherOutWarehouseCodeD.Name = "OtherOutWarehouseCodeD";
            this.OtherOutWarehouseCodeD.ReadOnly = true;
            this.OtherOutWarehouseCodeD.Visible = false;
            this.OtherOutWarehouseCodeD.Width = 81;
            // 
            // OtherOutWarehouseDetailCode
            // 
            this.OtherOutWarehouseDetailCode.DataPropertyName = "OtherOutWarehouseDetailCode";
            this.OtherOutWarehouseDetailCode.HeaderText = "出库明细单号";
            this.OtherOutWarehouseDetailCode.MinimumWidth = 9;
            this.OtherOutWarehouseDetailCode.Name = "OtherOutWarehouseDetailCode";
            this.OtherOutWarehouseDetailCode.ReadOnly = true;
            this.OtherOutWarehouseDetailCode.Visible = false;
            this.OtherOutWarehouseDetailCode.Width = 105;
            // 
            // TypeCode
            // 
            this.TypeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TypeCode.FormattingEnabled = true;
            this.TypeCode.Location = new System.Drawing.Point(320, 8);
            this.TypeCode.Name = "TypeCode";
            this.TypeCode.Size = new System.Drawing.Size(130, 25);
            this.TypeCode.TabIndex = 21;
            // 
            // MakeUser
            // 
            this.MakeUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MakeUser.FormattingEnabled = true;
            this.MakeUser.Location = new System.Drawing.Point(100, 45);
            this.MakeUser.Name = "MakeUser";
            this.MakeUser.Size = new System.Drawing.Size(130, 25);
            this.MakeUser.TabIndex = 24;
            // 
            // Frmw_OtherOutWarehouse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_OtherOutWarehouse";
            this.Text = "Frmw_OtherOutWarehouse";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Label lblTypeCode;
        private DateTimePicker InvoicesDate;
        private Label lblInvoicesDate;
        private TextBox InvoicesImage;
        private Label lblInvoicesImage;
        private TextBox Remark;
        private Label lblRemark;
        private TextBox Status;
        private Label lblStatus;
        private Label lblMakeUserID;
        private TextBox OtherOutWarehouseCode;
        private Label lblOtherOutWarehouseCode;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private ComboBox TypeCode;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewComboBoxColumn Warehouse;
        private DataGridViewTextBoxColumn GoodsSpec;
        private DataGridViewTextBoxColumn GoodsUnit;
        private DataGridViewTextBoxColumn Number;
        private DataGridViewTextBoxColumn RemarkD;
        private DataGridViewTextBoxColumn OtherOutWarehouseCodeD;
        private DataGridViewTextBoxColumn OtherOutWarehouseDetailCode;
        private DataGridViewTextBoxColumn GoodsCode;
        private DataGridViewComboBoxColumn GoodsName;
        private ComboBox MakeUser;
    }
}