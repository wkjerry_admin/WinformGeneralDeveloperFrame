namespace WinformDevFramework
{
    partial class FrmW_Goods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmW_Goods));
            this.lblGoodsCode = new System.Windows.Forms.Label();
            this.GoodsCode = new System.Windows.Forms.TextBox();
            this.lblGoodsName = new System.Windows.Forms.Label();
            this.GoodsName = new System.Windows.Forms.TextBox();
            this.lblGoodsSpec = new System.Windows.Forms.Label();
            this.GoodsSpec = new System.Windows.Forms.TextBox();
            this.lblGoodsType = new System.Windows.Forms.Label();
            this.lblGoodsUnit = new System.Windows.Forms.Label();
            this.lblRestockingPrice = new System.Windows.Forms.Label();
            this.RestockingPrice = new System.Windows.Forms.NumericUpDown();
            this.lblRetailPrice = new System.Windows.Forms.Label();
            this.RetailPrice = new System.Windows.Forms.NumericUpDown();
            this.lblWholesalePrice = new System.Windows.Forms.Label();
            this.WholesalePrice = new System.Windows.Forms.NumericUpDown();
            this.lblSafetyStock = new System.Windows.Forms.Label();
            this.SafetyStock = new System.Windows.Forms.NumericUpDown();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.lblGoodsImages = new System.Windows.Forms.Label();
            this.GoodsType = new System.Windows.Forms.ComboBox();
            this.GoodsUnit = new System.Windows.Forms.ComboBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewBuyDetail = new System.Windows.Forms.DataGridView();
            this.BuyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsNameD = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPriceD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalTaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.TotalStock = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GoodsImages = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RestockingPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetailPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WholesalePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SafetyStock)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBuyDetail)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsImages)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Size = new System.Drawing.Size(789, 31);
            // 
            // tabControl1
            // 
            this.tabControl1.Size = new System.Drawing.Size(789, 451);
            // 
            // tabList
            // 
            this.tabList.Size = new System.Drawing.Size(781, 421);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Size = new System.Drawing.Size(781, 421);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.GoodsImages);
            this.groupBox1.Controls.Add(this.TotalStock);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.GoodsUnit);
            this.groupBox1.Controls.Add(this.GoodsType);
            this.groupBox1.Controls.Add(this.lblGoodsImages);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.SafetyStock);
            this.groupBox1.Controls.Add(this.lblSafetyStock);
            this.groupBox1.Controls.Add(this.WholesalePrice);
            this.groupBox1.Controls.Add(this.lblWholesalePrice);
            this.groupBox1.Controls.Add(this.RetailPrice);
            this.groupBox1.Controls.Add(this.lblRetailPrice);
            this.groupBox1.Controls.Add(this.RestockingPrice);
            this.groupBox1.Controls.Add(this.lblRestockingPrice);
            this.groupBox1.Controls.Add(this.lblGoodsUnit);
            this.groupBox1.Controls.Add(this.lblGoodsType);
            this.groupBox1.Controls.Add(this.GoodsSpec);
            this.groupBox1.Controls.Add(this.lblGoodsSpec);
            this.groupBox1.Controls.Add(this.GoodsName);
            this.groupBox1.Controls.Add(this.lblGoodsName);
            this.groupBox1.Controls.Add(this.GoodsCode);
            this.groupBox1.Controls.Add(this.lblGoodsCode);
            this.groupBox1.Size = new System.Drawing.Size(775, 415);
            this.groupBox1.Controls.SetChildIndex(this.lblGoodsCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.GoodsCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblGoodsName, 0);
            this.groupBox1.Controls.SetChildIndex(this.GoodsName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblGoodsSpec, 0);
            this.groupBox1.Controls.SetChildIndex(this.GoodsSpec, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblGoodsType, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblGoodsUnit, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRestockingPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.RestockingPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRetailPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.RetailPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblWholesalePrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.WholesalePrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSafetyStock, 0);
            this.groupBox1.Controls.SetChildIndex(this.SafetyStock, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblGoodsImages, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.GoodsType, 0);
            this.groupBox1.Controls.SetChildIndex(this.GoodsUnit, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalStock, 0);
            this.groupBox1.Controls.SetChildIndex(this.GoodsImages, 0);
            this.groupBox1.Controls.SetChildIndex(this.button1, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Size = new System.Drawing.Size(785, 27);
            // 
            // lblGoodsCode
            // 
            this.lblGoodsCode.Location = new System.Drawing.Point(10, 10);
            this.lblGoodsCode.Name = "lblGoodsCode";
            this.lblGoodsCode.Size = new System.Drawing.Size(85, 23);
            this.lblGoodsCode.TabIndex = 2;
            this.lblGoodsCode.Text = "商品编号";
            this.lblGoodsCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GoodsCode
            // 
            this.GoodsCode.Location = new System.Drawing.Point(100, 10);
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.Size = new System.Drawing.Size(130, 23);
            this.GoodsCode.TabIndex = 1;
            // 
            // lblGoodsName
            // 
            this.lblGoodsName.Location = new System.Drawing.Point(230, 10);
            this.lblGoodsName.Name = "lblGoodsName";
            this.lblGoodsName.Size = new System.Drawing.Size(85, 23);
            this.lblGoodsName.TabIndex = 4;
            this.lblGoodsName.Text = "商品名称";
            this.lblGoodsName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GoodsName
            // 
            this.GoodsName.Location = new System.Drawing.Point(320, 10);
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.Size = new System.Drawing.Size(130, 23);
            this.GoodsName.TabIndex = 3;
            // 
            // lblGoodsSpec
            // 
            this.lblGoodsSpec.Location = new System.Drawing.Point(450, 10);
            this.lblGoodsSpec.Name = "lblGoodsSpec";
            this.lblGoodsSpec.Size = new System.Drawing.Size(85, 23);
            this.lblGoodsSpec.TabIndex = 6;
            this.lblGoodsSpec.Text = "规格型号";
            this.lblGoodsSpec.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.Location = new System.Drawing.Point(540, 10);
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.Size = new System.Drawing.Size(130, 23);
            this.GoodsSpec.TabIndex = 5;
            // 
            // lblGoodsType
            // 
            this.lblGoodsType.Location = new System.Drawing.Point(10, 45);
            this.lblGoodsType.Name = "lblGoodsType";
            this.lblGoodsType.Size = new System.Drawing.Size(85, 23);
            this.lblGoodsType.TabIndex = 8;
            this.lblGoodsType.Text = "商品类别";
            this.lblGoodsType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGoodsUnit
            // 
            this.lblGoodsUnit.Location = new System.Drawing.Point(230, 45);
            this.lblGoodsUnit.Name = "lblGoodsUnit";
            this.lblGoodsUnit.Size = new System.Drawing.Size(85, 23);
            this.lblGoodsUnit.TabIndex = 10;
            this.lblGoodsUnit.Text = "计量单位";
            this.lblGoodsUnit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRestockingPrice
            // 
            this.lblRestockingPrice.Location = new System.Drawing.Point(450, 45);
            this.lblRestockingPrice.Name = "lblRestockingPrice";
            this.lblRestockingPrice.Size = new System.Drawing.Size(85, 23);
            this.lblRestockingPrice.TabIndex = 12;
            this.lblRestockingPrice.Text = "进货价";
            this.lblRestockingPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RestockingPrice
            // 
            this.RestockingPrice.DecimalPlaces = 2;
            this.RestockingPrice.Location = new System.Drawing.Point(540, 45);
            this.RestockingPrice.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.RestockingPrice.Name = "RestockingPrice";
            this.RestockingPrice.Size = new System.Drawing.Size(130, 23);
            this.RestockingPrice.TabIndex = 11;
            // 
            // lblRetailPrice
            // 
            this.lblRetailPrice.Location = new System.Drawing.Point(10, 80);
            this.lblRetailPrice.Name = "lblRetailPrice";
            this.lblRetailPrice.Size = new System.Drawing.Size(85, 23);
            this.lblRetailPrice.TabIndex = 14;
            this.lblRetailPrice.Text = "零售价";
            this.lblRetailPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RetailPrice
            // 
            this.RetailPrice.Location = new System.Drawing.Point(100, 80);
            this.RetailPrice.Name = "RetailPrice";
            this.RetailPrice.Size = new System.Drawing.Size(130, 23);
            this.RetailPrice.TabIndex = 13;
            // 
            // lblWholesalePrice
            // 
            this.lblWholesalePrice.Location = new System.Drawing.Point(230, 80);
            this.lblWholesalePrice.Name = "lblWholesalePrice";
            this.lblWholesalePrice.Size = new System.Drawing.Size(85, 23);
            this.lblWholesalePrice.TabIndex = 16;
            this.lblWholesalePrice.Text = "批发价";
            this.lblWholesalePrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WholesalePrice
            // 
            this.WholesalePrice.Location = new System.Drawing.Point(320, 80);
            this.WholesalePrice.Name = "WholesalePrice";
            this.WholesalePrice.Size = new System.Drawing.Size(130, 23);
            this.WholesalePrice.TabIndex = 15;
            // 
            // lblSafetyStock
            // 
            this.lblSafetyStock.Location = new System.Drawing.Point(450, 80);
            this.lblSafetyStock.Name = "lblSafetyStock";
            this.lblSafetyStock.Size = new System.Drawing.Size(85, 23);
            this.lblSafetyStock.TabIndex = 18;
            this.lblSafetyStock.Text = "安全库存";
            this.lblSafetyStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SafetyStock
            // 
            this.SafetyStock.Location = new System.Drawing.Point(540, 80);
            this.SafetyStock.Name = "SafetyStock";
            this.SafetyStock.Size = new System.Drawing.Size(130, 23);
            this.SafetyStock.TabIndex = 17;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 115);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 20;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 115);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 19;
            // 
            // lblGoodsImages
            // 
            this.lblGoodsImages.Location = new System.Drawing.Point(230, 115);
            this.lblGoodsImages.Name = "lblGoodsImages";
            this.lblGoodsImages.Size = new System.Drawing.Size(85, 23);
            this.lblGoodsImages.TabIndex = 22;
            this.lblGoodsImages.Text = "商品图片";
            this.lblGoodsImages.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GoodsType
            // 
            this.GoodsType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GoodsType.FormattingEnabled = true;
            this.GoodsType.Location = new System.Drawing.Point(100, 45);
            this.GoodsType.Name = "GoodsType";
            this.GoodsType.Size = new System.Drawing.Size(130, 25);
            this.GoodsType.TabIndex = 23;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GoodsUnit.FormattingEnabled = true;
            this.GoodsUnit.Location = new System.Drawing.Point(321, 45);
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.Size = new System.Drawing.Size(130, 25);
            this.GoodsUnit.TabIndex = 24;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Location = new System.Drawing.Point(0, 171);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(778, 247);
            this.tabControl2.TabIndex = 33;
            this.tabControl2.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewBuyDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(770, 217);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "购货";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewBuyDetail
            // 
            this.dataGridViewBuyDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewBuyDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewBuyDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBuyDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BuyID,
            this.BuyCodeD,
            this.BuyDetailCode,
            this.GoodsNameD,
            this.GoodsCodeD,
            this.BuyNumber,
            this.UnitPrice,
            this.DiscountPrice,
            this.TotalPriceD,
            this.TaxRate,
            this.TaxUnitPrice,
            this.TaxPrice,
            this.TotalTaxPrice,
            this.RemarkD});
            this.dataGridViewBuyDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBuyDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewBuyDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewBuyDetail.Name = "dataGridViewBuyDetail";
            this.dataGridViewBuyDetail.ReadOnly = true;
            this.dataGridViewBuyDetail.RowHeadersVisible = false;
            this.dataGridViewBuyDetail.RowHeadersWidth = 62;
            this.dataGridViewBuyDetail.RowTemplate.Height = 25;
            this.dataGridViewBuyDetail.Size = new System.Drawing.Size(764, 211);
            this.dataGridViewBuyDetail.TabIndex = 1;
            // 
            // BuyID
            // 
            this.BuyID.DataPropertyName = "BuyID";
            this.BuyID.HeaderText = "采购单ID";
            this.BuyID.MinimumWidth = 8;
            this.BuyID.Name = "BuyID";
            this.BuyID.ReadOnly = true;
            this.BuyID.Visible = false;
            this.BuyID.Width = 63;
            // 
            // BuyCodeD
            // 
            this.BuyCodeD.DataPropertyName = "BuyCode";
            this.BuyCodeD.HeaderText = "采购单号";
            this.BuyCodeD.MinimumWidth = 8;
            this.BuyCodeD.Name = "BuyCodeD";
            this.BuyCodeD.ReadOnly = true;
            this.BuyCodeD.Width = 81;
            // 
            // BuyDetailCode
            // 
            this.BuyDetailCode.DataPropertyName = "BuyDetailCode";
            this.BuyDetailCode.HeaderText = "采购明细单号";
            this.BuyDetailCode.MinimumWidth = 8;
            this.BuyDetailCode.Name = "BuyDetailCode";
            this.BuyDetailCode.ReadOnly = true;
            this.BuyDetailCode.Width = 105;
            // 
            // GoodsNameD
            // 
            this.GoodsNameD.HeaderText = "商品名称";
            this.GoodsNameD.MinimumWidth = 8;
            this.GoodsNameD.Name = "GoodsNameD";
            this.GoodsNameD.ReadOnly = true;
            this.GoodsNameD.Width = 62;
            // 
            // GoodsCodeD
            // 
            this.GoodsCodeD.DataPropertyName = "GoodsCode";
            this.GoodsCodeD.HeaderText = "商品编码";
            this.GoodsCodeD.MinimumWidth = 8;
            this.GoodsCodeD.Name = "GoodsCodeD";
            this.GoodsCodeD.ReadOnly = true;
            this.GoodsCodeD.Width = 81;
            // 
            // BuyNumber
            // 
            this.BuyNumber.DataPropertyName = "BuyNumber";
            this.BuyNumber.HeaderText = "数量";
            this.BuyNumber.MinimumWidth = 8;
            this.BuyNumber.Name = "BuyNumber";
            this.BuyNumber.ReadOnly = true;
            this.BuyNumber.Width = 57;
            // 
            // UnitPrice
            // 
            this.UnitPrice.DataPropertyName = "UnitPrice";
            this.UnitPrice.HeaderText = "购货单价";
            this.UnitPrice.MinimumWidth = 8;
            this.UnitPrice.Name = "UnitPrice";
            this.UnitPrice.ReadOnly = true;
            this.UnitPrice.Width = 81;
            // 
            // DiscountPrice
            // 
            this.DiscountPrice.DataPropertyName = "DiscountPrice";
            this.DiscountPrice.HeaderText = "折扣金额";
            this.DiscountPrice.MinimumWidth = 8;
            this.DiscountPrice.Name = "DiscountPrice";
            this.DiscountPrice.ReadOnly = true;
            this.DiscountPrice.Width = 81;
            // 
            // TotalPriceD
            // 
            this.TotalPriceD.DataPropertyName = "TotalPrice";
            this.TotalPriceD.HeaderText = "金额";
            this.TotalPriceD.MinimumWidth = 8;
            this.TotalPriceD.Name = "TotalPriceD";
            this.TotalPriceD.ReadOnly = true;
            this.TotalPriceD.Width = 57;
            // 
            // TaxRate
            // 
            this.TaxRate.DataPropertyName = "TaxRate";
            this.TaxRate.HeaderText = "税率";
            this.TaxRate.MinimumWidth = 8;
            this.TaxRate.Name = "TaxRate";
            this.TaxRate.ReadOnly = true;
            this.TaxRate.Width = 57;
            // 
            // TaxUnitPrice
            // 
            this.TaxUnitPrice.DataPropertyName = "TaxUnitPrice";
            this.TaxUnitPrice.HeaderText = "含税单价";
            this.TaxUnitPrice.MinimumWidth = 8;
            this.TaxUnitPrice.Name = "TaxUnitPrice";
            this.TaxUnitPrice.ReadOnly = true;
            this.TaxUnitPrice.Width = 81;
            // 
            // TaxPrice
            // 
            this.TaxPrice.DataPropertyName = "TaxPrice";
            this.TaxPrice.HeaderText = "税额";
            this.TaxPrice.MinimumWidth = 8;
            this.TaxPrice.Name = "TaxPrice";
            this.TaxPrice.ReadOnly = true;
            this.TaxPrice.Width = 57;
            // 
            // TotalTaxPrice
            // 
            this.TotalTaxPrice.DataPropertyName = "TotalTaxPrice";
            this.TotalTaxPrice.HeaderText = "含税金额";
            this.TotalTaxPrice.MinimumWidth = 8;
            this.TotalTaxPrice.Name = "TotalTaxPrice";
            this.TotalTaxPrice.ReadOnly = true;
            this.TotalTaxPrice.Width = 81;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "RemarkD";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 8;
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.ReadOnly = true;
            this.RemarkD.Width = 57;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(781, 185);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "采购退货";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 25;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(775, 179);
            this.dataGridView2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(781, 185);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "销货";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.RowTemplate.Height = 25;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(775, 179);
            this.dataGridView3.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView4);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(781, 185);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "销货退货";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.Location = new System.Drawing.Point(3, 3);
            this.dataGridView4.MultiSelect = false;
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.RowHeadersVisible = false;
            this.dataGridView4.RowTemplate.Height = 25;
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(775, 179);
            this.dataGridView4.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 26);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(781, 185);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "其他入库";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 26);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(781, 185);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "其他出库";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // TotalStock
            // 
            this.TotalStock.Location = new System.Drawing.Point(540, 118);
            this.TotalStock.Name = "TotalStock";
            this.TotalStock.Size = new System.Drawing.Size(130, 23);
            this.TotalStock.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(450, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 23);
            this.label1.TabIndex = 35;
            this.label1.Text = "当前库存";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GoodsImages
            // 
            this.GoodsImages.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GoodsImages.Location = new System.Drawing.Point(320, 115);
            this.GoodsImages.Name = "GoodsImages";
            this.GoodsImages.Size = new System.Drawing.Size(63, 50);
            this.GoodsImages.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.GoodsImages.TabIndex = 36;
            this.GoodsImages.TabStop = false;
            this.GoodsImages.DoubleClick += new System.EventHandler(this.GoodsImages_DoubleClick);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(390, 118);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 23);
            this.button1.TabIndex = 37;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmW_Goods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 482);
            this.Name = "FrmW_Goods";
            this.Text = "FrmW_Goods";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RestockingPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetailPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WholesalePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SafetyStock)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBuyDetail)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsImages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
            private TextBox GoodsCode;
            private Label lblGoodsCode;
            private TextBox GoodsName;
            private Label lblGoodsName;
            private TextBox GoodsSpec;
            private Label lblGoodsSpec;
            private Label lblGoodsType;
            private Label lblGoodsUnit;
            private NumericUpDown RestockingPrice;
            private Label lblRestockingPrice;
            private NumericUpDown RetailPrice;
            private Label lblRetailPrice;
            private NumericUpDown WholesalePrice;
            private Label lblWholesalePrice;
            private NumericUpDown SafetyStock;
            private Label lblSafetyStock;
            private TextBox Remark;
            private Label lblRemark;
            private Label lblGoodsImages;
        private ComboBox GoodsType;
        private ComboBox GoodsUnit;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private DataGridView dataGridView2;
        private TabPage tabPage3;
        private DataGridView dataGridView3;
        private TabPage tabPage4;
        private DataGridView dataGridView4;
        private TextBox TotalStock;
        private Label label1;
        private TabPage tabPage5;
        private TabPage tabPage6;
        private PictureBox GoodsImages;
        private Button button1;
        private DataGridView dataGridViewBuyDetail;
        private DataGridViewTextBoxColumn BuyID;
        private DataGridViewTextBoxColumn BuyCodeD;
        private DataGridViewTextBoxColumn BuyDetailCode;
        private DataGridViewComboBoxColumn GoodsNameD;
        private DataGridViewTextBoxColumn GoodsCodeD;
        private DataGridViewTextBoxColumn BuyNumber;
        private DataGridViewTextBoxColumn UnitPrice;
        private DataGridViewTextBoxColumn DiscountPrice;
        private DataGridViewTextBoxColumn TotalPriceD;
        private DataGridViewTextBoxColumn TaxRate;
        private DataGridViewTextBoxColumn TaxUnitPrice;
        private DataGridViewTextBoxColumn TaxPrice;
        private DataGridViewTextBoxColumn TotalTaxPrice;
        private DataGridViewTextBoxColumn RemarkD;
    }
}