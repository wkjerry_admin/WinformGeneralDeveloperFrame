namespace WinformDevFramework
{
    partial class Frmw_Warehouse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblWarehouseName = new System.Windows.Forms.Label();
            this.WarehouseName = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.Address = new System.Windows.Forms.TextBox();
            this.lblStorageFee = new System.Windows.Forms.Label();
            this.StorageFee = new System.Windows.Forms.NumericUpDown();
            this.lblChargePersonID = new System.Windows.Forms.Label();
            this.lblSortOrder = new System.Windows.Forms.Label();
            this.SortOrder = new System.Windows.Forms.NumericUpDown();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.comboBoxPerson = new System.Windows.Forms.ComboBox();
            this.WarehoursesCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StorageFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SortOrder)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Size = new System.Drawing.Size(810, 31);
            // 
            // tabControl1
            // 
            this.tabControl1.Size = new System.Drawing.Size(810, 510);
            // 
            // tabList
            // 
            this.tabList.Size = new System.Drawing.Size(802, 480);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Size = new System.Drawing.Size(802, 480);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.WarehoursesCode);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxPerson);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.SortOrder);
            this.groupBox1.Controls.Add(this.lblSortOrder);
            this.groupBox1.Controls.Add(this.lblChargePersonID);
            this.groupBox1.Controls.Add(this.StorageFee);
            this.groupBox1.Controls.Add(this.lblStorageFee);
            this.groupBox1.Controls.Add(this.Address);
            this.groupBox1.Controls.Add(this.lblAddress);
            this.groupBox1.Controls.Add(this.WarehouseName);
            this.groupBox1.Controls.Add(this.lblWarehouseName);
            this.groupBox1.Size = new System.Drawing.Size(796, 474);
            this.groupBox1.Controls.SetChildIndex(this.lblWarehouseName, 0);
            this.groupBox1.Controls.SetChildIndex(this.WarehouseName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblAddress, 0);
            this.groupBox1.Controls.SetChildIndex(this.Address, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblStorageFee, 0);
            this.groupBox1.Controls.SetChildIndex(this.StorageFee, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblChargePersonID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSortOrder, 0);
            this.groupBox1.Controls.SetChildIndex(this.SortOrder, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.comboBoxPerson, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.WarehoursesCode, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Size = new System.Drawing.Size(806, 27);
            // 
            // lblWarehouseName
            // 
            this.lblWarehouseName.Location = new System.Drawing.Point(10, 32);
            this.lblWarehouseName.Name = "lblWarehouseName";
            this.lblWarehouseName.Size = new System.Drawing.Size(100, 23);
            this.lblWarehouseName.TabIndex = 2;
            this.lblWarehouseName.Text = "仓库名称";
            this.lblWarehouseName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WarehouseName
            // 
            this.WarehouseName.Location = new System.Drawing.Point(115, 32);
            this.WarehouseName.Name = "WarehouseName";
            this.WarehouseName.Size = new System.Drawing.Size(150, 23);
            this.WarehouseName.TabIndex = 1;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(10, 76);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(100, 23);
            this.lblAddress.TabIndex = 4;
            this.lblAddress.Text = "地址";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Address
            // 
            this.Address.Location = new System.Drawing.Point(115, 76);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(155, 23);
            this.Address.TabIndex = 3;
            // 
            // lblStorageFee
            // 
            this.lblStorageFee.Location = new System.Drawing.Point(267, 75);
            this.lblStorageFee.Name = "lblStorageFee";
            this.lblStorageFee.Size = new System.Drawing.Size(100, 23);
            this.lblStorageFee.TabIndex = 6;
            this.lblStorageFee.Text = "仓储费";
            this.lblStorageFee.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // StorageFee
            // 
            this.StorageFee.Location = new System.Drawing.Point(372, 75);
            this.StorageFee.Name = "StorageFee";
            this.StorageFee.Size = new System.Drawing.Size(100, 23);
            this.StorageFee.TabIndex = 5;
            // 
            // lblChargePersonID
            // 
            this.lblChargePersonID.Location = new System.Drawing.Point(499, 34);
            this.lblChargePersonID.Name = "lblChargePersonID";
            this.lblChargePersonID.Size = new System.Drawing.Size(100, 23);
            this.lblChargePersonID.TabIndex = 8;
            this.lblChargePersonID.Text = "负责人";
            this.lblChargePersonID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSortOrder
            // 
            this.lblSortOrder.Location = new System.Drawing.Point(522, 76);
            this.lblSortOrder.Name = "lblSortOrder";
            this.lblSortOrder.Size = new System.Drawing.Size(100, 23);
            this.lblSortOrder.TabIndex = 10;
            this.lblSortOrder.Text = "排序";
            this.lblSortOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SortOrder
            // 
            this.SortOrder.Location = new System.Drawing.Point(627, 76);
            this.SortOrder.Name = "SortOrder";
            this.SortOrder.Size = new System.Drawing.Size(100, 23);
            this.SortOrder.TabIndex = 9;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 120);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(100, 23);
            this.lblRemark.TabIndex = 12;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(115, 120);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(410, 23);
            this.Remark.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(475, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "元/天";
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(0, 149);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(799, 328);
            this.tabControl2.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(791, 298);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "仓储明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(785, 292);
            this.dataGridView1.TabIndex = 1;
            // 
            // comboBoxPerson
            // 
            this.comboBoxPerson.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPerson.FormattingEnabled = true;
            this.comboBoxPerson.Location = new System.Drawing.Point(605, 32);
            this.comboBoxPerson.Name = "comboBoxPerson";
            this.comboBoxPerson.Size = new System.Drawing.Size(122, 25);
            this.comboBoxPerson.TabIndex = 15;
            // 
            // WarehoursesCode
            // 
            this.WarehoursesCode.Location = new System.Drawing.Point(369, 32);
            this.WarehoursesCode.Name = "WarehoursesCode";
            this.WarehoursesCode.Size = new System.Drawing.Size(150, 23);
            this.WarehoursesCode.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(264, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 17;
            this.label2.Text = "仓库编码";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Frmw_Warehouse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 541);
            this.Name = "Frmw_Warehouse";
            this.Text = "Frmw_Warehouse";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StorageFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SortOrder)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
            private TextBox WarehouseName;
            private Label lblWarehouseName;
            private TextBox Address;
            private Label lblAddress;
            private NumericUpDown StorageFee;
            private Label lblStorageFee;
            private Label lblChargePersonID;
            private NumericUpDown SortOrder;
            private Label lblSortOrder;
            private TextBox Remark;
            private Label lblRemark;
        private Label label1;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridView1;
        private ComboBox comboBoxPerson;
        private TextBox WarehoursesCode;
        private Label label2;
    }
}