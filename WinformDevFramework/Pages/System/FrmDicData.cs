﻿using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;

namespace WinformDevFramework
{
    public partial class FrmDicData : BaseForm1
    {
        private IsysDicDataServices _sysDicDataServices;
        private IsysDicTypeServices _sysDicTypeServices;
        public FrmDicData(IsysDicTypeServices sysDicTypeServices,IsysDicDataServices sysDicDataServices)
        {
            _sysDicTypeServices= sysDicTypeServices;
            _sysDicDataServices= sysDicDataServices;
            InitializeComponent();
        }

        public override void Init()
        {
            base.Init();
            this.dataGridViewList.DataSource = _sysDicDataServices.QueryMuch<sysDicData,sysDicType,sysDicData>((
                (data, type) =>new object[]
                {
                    JoinType.Left,data.DicTypeID==type.ID
                }),((data, type) =>new sysDicData()
            {
                ID = data.ID,
                DicData = data.DicData,
                DicTypeID = data.DicTypeID,
                DicTypeName = type.DicTypeName,
                Remark = data.Remark,
            } ));
            this.dataGridViewList.Columns["DicData"]!.HeaderText = "字典内容";
            this.dataGridViewList.Columns["DicTypeName"]!.HeaderText = "字典类型";
            this.dataGridViewList.Columns["DicTypeName"].DisplayIndex = 2;
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";
            this.dataGridViewList.Columns["DicTypeID"].Visible=false;
            SetToolButtonStatus(formStatus);
            SetDicType(_sysDicTypeServices.Query());
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as sysDicData;
            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            txtDicDataText.Text = model.DicData;
            txtRemark.Text = model.Remark;
            cmbDicType.Text = model.DicTypeName;
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    sysDicData newMenu = new sysDicData();
                    newMenu.DicData =txtDicDataText.Text;
                    newMenu.DicTypeID =(int) cmbDicType.SelectedValue;
                    newMenu.Remark = txtRemark.Text;
                    var id = _sysDicDataServices.Insert(newMenu);
                    txtID.Text = id.ToString();
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                // 修改
                else
                {
                    sysDicData newMenu = _sysDicDataServices.QueryById(int.Parse(txtID.Text));
                    newMenu.DicData = txtDicDataText.Text;
                    newMenu.DicTypeID = (int)cmbDicType.SelectedValue;
                    newMenu.Remark = txtRemark.Text;
                    if (_sysDicDataServices.Update(newMenu))
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                SetToolButtonStatus(formStatus);
                this.dataGridViewList.DataSource = _sysDicDataServices.QueryMuch<sysDicData, sysDicType, sysDicData>((
                    (data, type) => new object[]
                    {
                        JoinType.Left,data.DicTypeID==type.ID
                    }), ((data, type) => new sysDicData()
                {
                    ID = data.ID,
                    DicData = data.DicData,
                    DicTypeID = data.DicTypeID,
                    DicTypeName = type.DicTypeName,
                    Remark = data.Remark,
                })); ;
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                _sysDicDataServices.DeleteById(Int32.Parse(txtID.Text));
                formStatus = FormStatus.Del;
                SetToolButtonStatus(formStatus);
                this.dataGridViewList.DataSource = _sysDicDataServices.QueryMuch<sysDicData, sysDicType, sysDicData>((
                    (data, type) => new object[]
                    {
                        JoinType.Left,data.DicTypeID==type.ID
                    }), ((data, type) => new sysDicData()
                {
                    ID = data.ID,
                    DicData = data.DicData,
                    DicTypeID = data.DicTypeID,
                    DicTypeName = type.DicTypeName,
                    Remark = data.Remark,
                })); ;
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }
        /// <summary>
        /// 字典类型控件设置
        /// </summary>
        private void SetDicType(List<sysDicType> types)
        {
            cmbDicType.Items.Clear();
            cmbDicType.DataSource=types;
            cmbDicType.ValueMember = "ID";
            cmbDicType.SelectedIndex = 0;
            cmbDicType.DisplayMember = "DicTypeName";
        }

        public override bool ValidateData()
        {
            if (string.IsNullOrEmpty(txtDicDataText.Text))
            {
                MessageBox.Show("字典内容不能为空！");
                return false;
            }
            if (string.IsNullOrEmpty(cmbDicType.Text))
            {
                MessageBox.Show("字典类型不能为空！");
                return false;
            }

            return true;
        }
    }
}
