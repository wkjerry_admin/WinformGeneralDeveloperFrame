﻿namespace WinformDevFramework
{
    partial class FrmUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePickerCreateDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerLastLoginDate = new System.Windows.Forms.DateTimePicker();
            this.cmbSex = new System.Windows.Forms.ComboBox();
            this.cbStatus = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.DeptID = new Sunny.UI.UIComboTreeView();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DeptID);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbStatus);
            this.groupBox1.Controls.Add(this.cmbSex);
            this.groupBox1.Controls.Add(this.dateTimePickerLastLoginDate);
            this.groupBox1.Controls.Add(this.dateTimePickerCreateDate);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtPhoneNumber);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtMail);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtFullName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtUserName, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtFullName, 0);
            this.groupBox1.Controls.SetChildIndex(this.label3, 0);
            this.groupBox1.Controls.SetChildIndex(this.label4, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtMail, 0);
            this.groupBox1.Controls.SetChildIndex(this.label5, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtPhoneNumber, 0);
            this.groupBox1.Controls.SetChildIndex(this.label6, 0);
            this.groupBox1.Controls.SetChildIndex(this.label8, 0);
            this.groupBox1.Controls.SetChildIndex(this.label7, 0);
            this.groupBox1.Controls.SetChildIndex(this.dateTimePickerCreateDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.dateTimePickerLastLoginDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.cmbSex, 0);
            this.groupBox1.Controls.SetChildIndex(this.cbStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.label9, 0);
            this.groupBox1.Controls.SetChildIndex(this.DeptID, 0);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(35, 38);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(59, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "账号";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(100, 38);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 23);
            this.txtUserName.TabIndex = 2;
            // 
            // txtFullName
            // 
            this.txtFullName.Location = new System.Drawing.Point(291, 38);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(100, 23);
            this.txtFullName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(226, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "用户昵称";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(427, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "性别";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(100, 91);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(100, 23);
            this.txtMail.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(35, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "邮箱";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(291, 91);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(100, 23);
            this.txtPhoneNumber.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(226, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "手机号码";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(427, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 23);
            this.label6.TabIndex = 11;
            this.label6.Text = "状态";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(7, 211);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 23);
            this.label7.TabIndex = 15;
            this.label7.Text = "最后登录时间";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(35, 154);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 23);
            this.label8.TabIndex = 13;
            this.label8.Text = "创建时间";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateTimePickerCreateDate
            // 
            this.dateTimePickerCreateDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dateTimePickerCreateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerCreateDate.Location = new System.Drawing.Point(100, 154);
            this.dateTimePickerCreateDate.Name = "dateTimePickerCreateDate";
            this.dateTimePickerCreateDate.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerCreateDate.TabIndex = 16;
            // 
            // dateTimePickerLastLoginDate
            // 
            this.dateTimePickerLastLoginDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dateTimePickerLastLoginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerLastLoginDate.Location = new System.Drawing.Point(100, 211);
            this.dateTimePickerLastLoginDate.Name = "dateTimePickerLastLoginDate";
            this.dateTimePickerLastLoginDate.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerLastLoginDate.TabIndex = 17;
            // 
            // cmbSex
            // 
            this.cmbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSex.FormattingEnabled = true;
            this.cmbSex.Items.AddRange(new object[] {
            "男",
            "女"});
            this.cmbSex.Location = new System.Drawing.Point(492, 36);
            this.cmbSex.Name = "cmbSex";
            this.cmbSex.Size = new System.Drawing.Size(100, 25);
            this.cmbSex.TabIndex = 18;
            // 
            // cbStatus
            // 
            this.cbStatus.AutoSize = true;
            this.cbStatus.Location = new System.Drawing.Point(492, 156);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(75, 21);
            this.cbStatus.TabIndex = 19;
            this.cbStatus.Text = "是否启用";
            this.cbStatus.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(427, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 23);
            this.label9.TabIndex = 20;
            this.label9.Text = "部门";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DeptID
            // 
            this.DeptID.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            this.DeptID.FillColor = System.Drawing.Color.White;
            this.DeptID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.DeptID.Location = new System.Drawing.Point(493, 86);
            this.DeptID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DeptID.MinimumSize = new System.Drawing.Size(63, 0);
            this.DeptID.Name = "DeptID";
            this.DeptID.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.DeptID.RectColor = System.Drawing.Color.Gray;
            this.DeptID.Size = new System.Drawing.Size(150, 28);
            this.DeptID.Style = Sunny.UI.UIStyle.Custom;
            this.DeptID.TabIndex = 21;
            this.DeptID.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.DeptID.Watermark = "";
            // 
            // FrmUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "FrmUser";
            this.Text = "FrmUser";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox txtUserName;
        private Label label1;
        private TextBox txtFullName;
        private Label label2;
        private Label label3;
        private Label label6;
        private TextBox txtPhoneNumber;
        private Label label5;
        private TextBox txtMail;
        private Label label4;
        private Label label7;
        private Label label8;
        private DateTimePicker dateTimePickerCreateDate;
        private DateTimePicker dateTimePickerLastLoginDate;
        private ComboBox cmbSex;
        private CheckBox cbStatus;
        private Label label9;
        private Sunny.UI.UIComboTreeView DeptID;
    }
}