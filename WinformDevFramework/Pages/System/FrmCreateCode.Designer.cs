﻿namespace WinformDevFramework
{
    partial class FrmCreateCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNameSpace = new System.Windows.Forms.TextBox();
            this.uiComboTreeViewTable = new Sunny.UI.UIComboTreeView();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewTable = new System.Windows.Forms.DataGridView();
            this.tableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataBaseFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FieldType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.controlType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.controlName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isIdentity = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isSearch = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isEdit = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Controls.Add(this.button4);
            this.panel3.Controls.Add(this.btnOk);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.txtUrl);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.txtNameSpace);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.uiComboTreeViewTable);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(779, 426);
            this.panel3.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.splitContainer1);
            this.groupBox1.Location = new System.Drawing.Point(3, 248);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(773, 175);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "界面";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(306, 219);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(131, 23);
            this.button4.TabIndex = 16;
            this.button4.Text = "取消";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(150, 219);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(130, 23);
            this.btnOk.TabIndex = 15;
            this.btnOk.Text = "完成";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(443, 168);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "选择目录";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(150, 168);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.ReadOnly = true;
            this.txtUrl.Size = new System.Drawing.Size(287, 23);
            this.txtUrl.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(54, 168);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(90, 23);
            this.label5.TabIndex = 12;
            this.label5.Text = "输出目录";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.Location = new System.Drawing.Point(150, 137);
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.Size = new System.Drawing.Size(287, 23);
            this.txtNameSpace.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(54, 137);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(90, 23);
            this.label4.TabIndex = 10;
            this.label4.Text = "命名空间";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(54, 104);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(90, 23);
            this.label3.TabIndex = 8;
            this.label3.Text = "选择数据表";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(29, 68);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(106, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "数据库连接字符串";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(153, 68);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(284, 23);
            this.textBox1.TabIndex = 6;

            // uiComboTreeViewTable
            // 
            this.uiComboTreeViewTable.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            this.uiComboTreeViewTable.FillColor = System.Drawing.Color.White;
            this.uiComboTreeViewTable.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiComboTreeViewTable.Location = new System.Drawing.Point(153, 101);
            this.uiComboTreeViewTable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboTreeViewTable.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboTreeViewTable.Name = "uiComboTreeViewTable";
            this.uiComboTreeViewTable.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboTreeViewTable.RectColor = System.Drawing.Color.Gray;
            this.uiComboTreeViewTable.Size = new System.Drawing.Size(284, 25);
            this.uiComboTreeViewTable.Style = Sunny.UI.UIStyle.Custom;
            this.uiComboTreeViewTable.TabIndex = 9;
            this.uiComboTreeViewTable.TagString = "";
            this.uiComboTreeViewTable.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiComboTreeViewTable.Watermark = "";
            // 
            // 
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(362, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "连接";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(153, 36);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(186, 25);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(45, 38);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(90, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "选择数据库";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 19);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Size = new System.Drawing.Size(767, 153);
            this.splitContainer1.SplitterDistance = 133;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewTable);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(133, 153);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "数据表";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(630, 153);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "设置";
            // 
            // dataGridViewTable
            // 
            this.dataGridViewTable.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tableName});
            this.dataGridViewTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTable.Location = new System.Drawing.Point(3, 19);
            this.dataGridViewTable.Name = "dataGridViewTable";
            this.dataGridViewTable.ReadOnly = true;
            this.dataGridViewTable.RowHeadersVisible = false;
            this.dataGridViewTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridViewTable.RowTemplate.Height = 25;
            this.dataGridViewTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTable.Size = new System.Drawing.Size(127, 131);
            this.dataGridViewTable.TabIndex = 0;
            // 
            // tableName
            // 
            this.tableName.HeaderText = "表名";
            this.tableName.Name = "tableName";
            this.tableName.DataPropertyName = "tableName";
            this.tableName.ReadOnly = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataBaseFieldName,
            this.FieldType,
            this.controlType,
            this.controlName,
            this.labelName,
            this.labelText,
            this.isIdentity,
            this.isSearch,
            this.isCheck,
            this.isEdit});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = false;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(624, 131);
            this.dataGridView1.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumnFieldName
            // 
            this.dataBaseFieldName.HeaderText = "字段名称";
            this.dataBaseFieldName.Name = "dataBaseFieldName";
            this.dataBaseFieldName.DataPropertyName = "dataBaseFieldName";
            this.dataBaseFieldName.ReadOnly = false;
            // 
            // FieldType
            // 
            this.FieldType.HeaderText = "字段类型";
            this.FieldType.Name = "dataBaseFieldType";
            this.FieldType.DataPropertyName = "dataBaseFieldType";
            this.FieldType.ReadOnly = false;
            // 
            // controlType
            // 
            this.controlType.HeaderText = "控件类型";
            this.controlType.Name = "controlType";
            this.controlType.DataPropertyName = "controlType";
            this.controlType.ReadOnly = false;
            // 
            // controlName
            // 
            this.controlName.HeaderText = "控件Name";
            this.controlName.Name = "controlName";
            this.controlName.DataPropertyName = "controlName";
            this.controlName.ReadOnly = false;
            // 
            // labelName
            // 
            this.labelName.HeaderText = "标签Name";
            this.labelName.Name = "labelName";
            this.labelName.DataPropertyName = "labelName";
            this.labelName.ReadOnly = false;
            // 
            // labelText
            // 
            this.labelText.HeaderText = "标签文本";
            this.labelText.Name = "labelText";
            this.labelText.DataPropertyName = "labelText";
            this.labelText.ReadOnly = false;
            // 
            // isIdentity
            // 
            this.isIdentity.HeaderText = "是否自增";
            this.isIdentity.Name = "isIdentity";
            this.isIdentity.DataPropertyName = "isIdentity";
            this.isIdentity.ReadOnly = true;
            // 
            // isSearch
            // 
            this.isSearch.HeaderText = "是否搜索项";
            this.isSearch.Name = "isSearch";
            this.isSearch.DataPropertyName = "isSearch";
            this.isSearch.ReadOnly = false;
            // 
            // isCheck
            // 
            this.isCheck.HeaderText = "是否要为空检验";
            this.isCheck.Name = "isCheck";
            this.isCheck.DataPropertyName = "isCheck";
            this.isCheck.ReadOnly = false;
            // 
            // isEdit
            // 
            this.isEdit.HeaderText = "是否可编辑";
            this.isEdit.Name = "isEdit";
            this.isEdit.DataPropertyName = "isEdit";
            this.isEdit.ReadOnly = false;
            // 
            // FrmCreateCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 426);
            this.Controls.Add(this.panel3);
            this.Name = "FrmCreateCode";
            this.Text = "FrmCreateCode";
            this.Load += new System.EventHandler(this.FrmCreateCode_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel3;
        private Label label1;
        private ComboBox comboBox1;
        private Button button1;
        private TextBox textBox1;
        private Label label2;
        private Label label3;
        private Sunny.UI.UIComboTreeView uiComboTreeViewTable;
        private Label label4;
        private TextBox txtNameSpace;
        private TextBox txtUrl;
        private Label label5;
        private Button button2;
        private Button button4;
        private Button btnOk;
        private GroupBox groupBox1;
        private SplitContainer splitContainer1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private DataGridView dataGridViewTable;
        private DataGridViewTextBoxColumn tableName;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn dataBaseFieldName;
        private DataGridViewTextBoxColumn FieldType;
        private DataGridViewComboBoxColumn controlType;
        private DataGridViewTextBoxColumn controlName;
        private DataGridViewTextBoxColumn labelName;
        private DataGridViewTextBoxColumn labelText;
        private DataGridViewCheckBoxColumn isIdentity;
        private DataGridViewCheckBoxColumn isSearch;
        private DataGridViewCheckBoxColumn isCheck;
        private DataGridViewCheckBoxColumn isEdit;
    }
}