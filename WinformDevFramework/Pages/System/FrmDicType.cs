﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework.Models;
using static Sunny.UI.UIAvatar;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WinformDevFramework
{
    public partial class FrmDicType : BaseForm1
    {
        private IsysDicTypeServices _sysDicTypeServices;
        public FrmDicType(IsysDicTypeServices sysDicTypeServices)
        {
            _sysDicTypeServices= sysDicTypeServices;
            InitializeComponent();
        }

        public override void Init()
        {
            base.Init();
            this.dataGridViewList.DataSource = GetAllDicTypes();
            this.dataGridViewList.Columns["DicTypeName"]!.HeaderText = "类型名称";
            this.dataGridViewList.Columns["DicTypeCode"]!.HeaderText = "类型编码";
            this.dataGridViewList.Columns["Remark"]!.HeaderText = "备注";
            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as sysDicType;
            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            txtDicTypeCode.Text = model.DicTypeCode;
            txtDicTypeName.Text = model.DicTypeName;
            txtRemark.Text = model.Remark;
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            //if (ValidateData())
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    sysDicType newMenu = new sysDicType();
                    newMenu.DicTypeName = txtDicTypeName.Text;
                    newMenu.DicTypeCode = txtDicTypeCode.Text;
                    newMenu.Remark = txtRemark.Text;
                    var id = _sysDicTypeServices.Insert(newMenu);
                    txtID.Text = id.ToString();
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                // 修改
                else
                {
                    sysDicType newMenu = _sysDicTypeServices.QueryById(int.Parse(txtID.Text));
                    newMenu.DicTypeName = txtDicTypeName.Text;
                    newMenu.DicTypeCode = txtDicTypeCode.Text;
                    newMenu.Remark = txtRemark.Text;
                    if (_sysDicTypeServices.Update(newMenu))
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }

                SetToolButtonStatus(formStatus);
                var menus = GetAllDicTypes();
                this.dataGridViewList.DataSource = menus;
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                _sysDicTypeServices.DeleteById(Int32.Parse(txtID.Text));
                formStatus = FormStatus.Del;
                SetToolButtonStatus(formStatus);
                var menus = GetAllDicTypes();
                this.dataGridViewList.DataSource = menus;
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        /// <summary>
        /// 校验数据
        /// </summary>
        /// <returns></returns>
        public override bool ValidateData()
        {
            bool result = false;
            if (string.IsNullOrEmpty(txtDicTypeName.Text))
            {
                MessageBox.Show("类型名称不能为空！");
                return result;
            }
            if (string.IsNullOrEmpty(txtDicTypeCode.Text))
            {
                MessageBox.Show("类型编码不能为空！");
                return result;
            }
            result = true;
            return result;
        }

        private List<sysDicType> GetAllDicTypes()
        {
            return _sysDicTypeServices.Query();
        }

    }
}
