namespace WinformDevFramework
{
    partial class Frmw_OtherIncome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOtherIncomeCode = new System.Windows.Forms.Label();
            this.OtherIncomeCode = new System.Windows.Forms.TextBox();
            this.lblFlatCode = new System.Windows.Forms.Label();
            this.FlatCode = new System.Windows.Forms.TextBox();
            this.lblFlatName = new System.Windows.Forms.Label();
            this.lblInvicesDate = new System.Windows.Forms.Label();
            this.InvicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblSettlementCode = new System.Windows.Forms.Label();
            this.SettlementCode = new System.Windows.Forms.TextBox();
            this.lblSettlementName = new System.Windows.Forms.Label();
            this.lblIncomeTypeID = new System.Windows.Forms.Label();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.TotalPrice = new System.Windows.Forms.TextBox();
            this.lblMakeUserID = new System.Windows.Forms.Label();
            this.lblReviewUserID = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.ReviewUserID = new System.Windows.Forms.ComboBox();
            this.MakeUserID = new System.Windows.Forms.ComboBox();
            this.IncomeTypeID = new System.Windows.Forms.ComboBox();
            this.SettlementName = new System.Windows.Forms.ComboBox();
            this.FlatName = new System.Windows.Forms.ComboBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(2);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.FlatName);
            this.groupBox1.Controls.Add(this.SettlementName);
            this.groupBox1.Controls.Add(this.IncomeTypeID);
            this.groupBox1.Controls.Add(this.ReviewUserID);
            this.groupBox1.Controls.Add(this.MakeUserID);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.lblReviewUserID);
            this.groupBox1.Controls.Add(this.lblMakeUserID);
            this.groupBox1.Controls.Add(this.TotalPrice);
            this.groupBox1.Controls.Add(this.lblTotalPrice);
            this.groupBox1.Controls.Add(this.lblIncomeTypeID);
            this.groupBox1.Controls.Add(this.lblSettlementName);
            this.groupBox1.Controls.Add(this.SettlementCode);
            this.groupBox1.Controls.Add(this.lblSettlementCode);
            this.groupBox1.Controls.Add(this.InvicesDate);
            this.groupBox1.Controls.Add(this.lblInvicesDate);
            this.groupBox1.Controls.Add(this.lblFlatName);
            this.groupBox1.Controls.Add(this.FlatCode);
            this.groupBox1.Controls.Add(this.lblFlatCode);
            this.groupBox1.Controls.Add(this.OtherIncomeCode);
            this.groupBox1.Controls.Add(this.lblOtherIncomeCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(780, 379);
            this.groupBox1.Controls.SetChildIndex(this.lblOtherIncomeCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.OtherIncomeCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblFlatCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.FlatCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblFlatName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSettlementCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSettlementName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblIncomeTypeID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblReviewUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.Status, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.ReviewUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.IncomeTypeID, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementName, 0);
            this.groupBox1.Controls.SetChildIndex(this.FlatName, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(2);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Size = new System.Drawing.Size(53, 23);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(59, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(116, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Size = new System.Drawing.Size(53, 23);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(173, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCanel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(230, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnDel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(287, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetPW.Size = new System.Drawing.Size(53, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(401, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(344, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Size = new System.Drawing.Size(53, 23);
            // 
            // lblOtherIncomeCode
            // 
            this.lblOtherIncomeCode.Location = new System.Drawing.Point(10, 10);
            this.lblOtherIncomeCode.Name = "lblOtherIncomeCode";
            this.lblOtherIncomeCode.Size = new System.Drawing.Size(85, 23);
            this.lblOtherIncomeCode.TabIndex = 4;
            this.lblOtherIncomeCode.Text = "其他收入单号";
            this.lblOtherIncomeCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OtherIncomeCode
            // 
            this.OtherIncomeCode.Location = new System.Drawing.Point(100, 10);
            this.OtherIncomeCode.Name = "OtherIncomeCode";
            this.OtherIncomeCode.PlaceholderText = "为空自动生成";
            this.OtherIncomeCode.Size = new System.Drawing.Size(130, 23);
            this.OtherIncomeCode.TabIndex = 3;
            // 
            // lblFlatCode
            // 
            this.lblFlatCode.Location = new System.Drawing.Point(450, 10);
            this.lblFlatCode.Name = "lblFlatCode";
            this.lblFlatCode.Size = new System.Drawing.Size(85, 23);
            this.lblFlatCode.TabIndex = 6;
            this.lblFlatCode.Text = "对方单位编码";
            this.lblFlatCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FlatCode
            // 
            this.FlatCode.Location = new System.Drawing.Point(540, 10);
            this.FlatCode.Name = "FlatCode";
            this.FlatCode.Size = new System.Drawing.Size(130, 23);
            this.FlatCode.TabIndex = 5;
            // 
            // lblFlatName
            // 
            this.lblFlatName.Location = new System.Drawing.Point(230, 10);
            this.lblFlatName.Name = "lblFlatName";
            this.lblFlatName.Size = new System.Drawing.Size(85, 23);
            this.lblFlatName.TabIndex = 8;
            this.lblFlatName.Text = "对方单位名称";
            this.lblFlatName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblInvicesDate
            // 
            this.lblInvicesDate.Location = new System.Drawing.Point(450, 45);
            this.lblInvicesDate.Name = "lblInvicesDate";
            this.lblInvicesDate.Size = new System.Drawing.Size(85, 23);
            this.lblInvicesDate.TabIndex = 10;
            this.lblInvicesDate.Text = "单据日期";
            this.lblInvicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvicesDate
            // 
            this.InvicesDate.Location = new System.Drawing.Point(540, 45);
            this.InvicesDate.Name = "InvicesDate";
            this.InvicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvicesDate.TabIndex = 9;
            // 
            // lblSettlementCode
            // 
            this.lblSettlementCode.Location = new System.Drawing.Point(230, 45);
            this.lblSettlementCode.Name = "lblSettlementCode";
            this.lblSettlementCode.Size = new System.Drawing.Size(85, 23);
            this.lblSettlementCode.TabIndex = 12;
            this.lblSettlementCode.Text = "结算账户编码";
            this.lblSettlementCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SettlementCode
            // 
            this.SettlementCode.Location = new System.Drawing.Point(320, 45);
            this.SettlementCode.Name = "SettlementCode";
            this.SettlementCode.Size = new System.Drawing.Size(130, 23);
            this.SettlementCode.TabIndex = 11;
            // 
            // lblSettlementName
            // 
            this.lblSettlementName.Location = new System.Drawing.Point(10, 45);
            this.lblSettlementName.Name = "lblSettlementName";
            this.lblSettlementName.Size = new System.Drawing.Size(85, 23);
            this.lblSettlementName.TabIndex = 14;
            this.lblSettlementName.Text = "结算账户名称";
            this.lblSettlementName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblIncomeTypeID
            // 
            this.lblIncomeTypeID.Location = new System.Drawing.Point(10, 80);
            this.lblIncomeTypeID.Name = "lblIncomeTypeID";
            this.lblIncomeTypeID.Size = new System.Drawing.Size(85, 23);
            this.lblIncomeTypeID.TabIndex = 16;
            this.lblIncomeTypeID.Text = "收入类别";
            this.lblIncomeTypeID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.Location = new System.Drawing.Point(230, 80);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice.TabIndex = 20;
            this.lblTotalPrice.Text = "金额";
            this.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TotalPrice
            // 
            this.TotalPrice.Location = new System.Drawing.Point(320, 80);
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice.TabIndex = 19;
            // 
            // lblMakeUserID
            // 
            this.lblMakeUserID.Location = new System.Drawing.Point(450, 80);
            this.lblMakeUserID.Name = "lblMakeUserID";
            this.lblMakeUserID.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUserID.TabIndex = 22;
            this.lblMakeUserID.Text = "制单人";
            this.lblMakeUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblReviewUserID
            // 
            this.lblReviewUserID.Location = new System.Drawing.Point(230, 115);
            this.lblReviewUserID.Name = "lblReviewUserID";
            this.lblReviewUserID.Size = new System.Drawing.Size(85, 23);
            this.lblReviewUserID.TabIndex = 24;
            this.lblReviewUserID.Text = "审核人";
            this.lblReviewUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblReviewUserID.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(450, 115);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(85, 23);
            this.lblStatus.TabIndex = 26;
            this.lblStatus.Text = "单据状态";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblStatus.Visible = false;
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(540, 115);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(130, 23);
            this.Status.TabIndex = 25;
            this.Status.Visible = false;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 115);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 28;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 115);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 27;
            // 
            // ReviewUserID
            // 
            this.ReviewUserID.FormattingEnabled = true;
            this.ReviewUserID.Location = new System.Drawing.Point(320, 115);
            this.ReviewUserID.Name = "ReviewUserID";
            this.ReviewUserID.Size = new System.Drawing.Size(130, 25);
            this.ReviewUserID.TabIndex = 35;
            this.ReviewUserID.Visible = false;
            // 
            // MakeUserID
            // 
            this.MakeUserID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MakeUserID.FormattingEnabled = true;
            this.MakeUserID.Location = new System.Drawing.Point(540, 80);
            this.MakeUserID.Name = "MakeUserID";
            this.MakeUserID.Size = new System.Drawing.Size(130, 25);
            this.MakeUserID.TabIndex = 34;
            // 
            // IncomeTypeID
            // 
            this.IncomeTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IncomeTypeID.FormattingEnabled = true;
            this.IncomeTypeID.Location = new System.Drawing.Point(100, 80);
            this.IncomeTypeID.Name = "IncomeTypeID";
            this.IncomeTypeID.Size = new System.Drawing.Size(130, 25);
            this.IncomeTypeID.TabIndex = 36;
            // 
            // SettlementName
            // 
            this.SettlementName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SettlementName.FormattingEnabled = true;
            this.SettlementName.Location = new System.Drawing.Point(100, 45);
            this.SettlementName.Name = "SettlementName";
            this.SettlementName.Size = new System.Drawing.Size(130, 25);
            this.SettlementName.TabIndex = 37;
            // 
            // FlatName
            // 
            this.FlatName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FlatName.FormattingEnabled = true;
            this.FlatName.Location = new System.Drawing.Point(321, 10);
            this.FlatName.Name = "FlatName";
            this.FlatName.Size = new System.Drawing.Size(130, 25);
            this.FlatName.TabIndex = 38;
            // 
            // Frmw_OtherIncome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_OtherIncome";
            this.Text = "Frmw_OtherIncome";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private NumericUpDown ID;
        private Label lblID;
        private TextBox OtherIncomeCode;
        private Label lblOtherIncomeCode;
        private TextBox FlatCode;
        private Label lblFlatCode;
        private Label lblFlatName;
        private DateTimePicker InvicesDate;
        private Label lblInvicesDate;
        private TextBox SettlementCode;
        private Label lblSettlementCode;
        private Label lblSettlementName;
        private Label lblIncomeTypeID;
        private TextBox IncomeType;
        private Label lblIncomeType;
        private TextBox TotalPrice;
        private Label lblTotalPrice;
        private Label lblMakeUserID;
        private Label lblReviewUserID;
        private TextBox Status;
        private Label lblStatus;
        private TextBox Remark;
        private Label lblRemark;
        private ComboBox ReviewUserID;
        private ComboBox MakeUserID;
        private ComboBox IncomeTypeID;
        private ComboBox SettlementName;
        private ComboBox FlatName;
    }
}