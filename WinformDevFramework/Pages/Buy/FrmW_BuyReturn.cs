using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;

namespace WinformDevFramework
{
    public partial class Frmw_BuyReturn : BaseForm1
    {
        private Iw_BuyReturnServices _w_BuyReturnServices;
        private Iw_BuyServices _w_BuyServices;
        private Iw_BuyDetailServices _w_BuyDetailServices;
        private IW_GoodsServices _w_GoodsServices;
        private Iw_BuyReturnDetailServices _w_BuyReturnDetailServices;
        private Iw_BuyReturnOutWarehouseDetailServices _w_buyReturnOutWarehouseDetailServices;
        public Frmw_BuyReturn(Iw_BuyReturnServices w_BuyReturnServices, Iw_BuyServices w_BuyServices,Iw_BuyDetailServices w_BuyDetailServices, IW_GoodsServices w_GoodsServices, Iw_BuyReturnDetailServices w_BuyReturnDetailServices, Iw_BuyReturnOutWarehouseDetailServices w_buyReturnOutWarehouseDetailServices)
        {
            _w_BuyReturnServices = w_BuyReturnServices;
            _w_BuyDetailServices = w_BuyDetailServices;
            _w_GoodsServices = w_GoodsServices;
            InitializeComponent();
            _w_BuyServices = w_BuyServices;
            _w_BuyReturnDetailServices = w_BuyReturnDetailServices;
            _w_buyReturnOutWarehouseDetailServices = w_buyReturnOutWarehouseDetailServices;
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = GetData();
           
            // 设置隐藏列
            this.dataGridViewList.Columns["BuyReturnCode"].HeaderText = "退货单号";
            this.dataGridViewList.Columns["BuyCode"].HeaderText = "关联的采购单号";
            this.dataGridViewList.Columns["SupplierCode"].Visible = false;
            this.dataGridViewList.Columns["SupplierName"].HeaderText = "供应商";
            this.dataGridViewList.Columns["InvoicesDate"].HeaderText = "单据日期";
            this.dataGridViewList.Columns["MakeUserName"].HeaderText = "制单人";
            this.dataGridViewList.Columns["ReviewUserName"].HeaderText = "审核人";
            this.dataGridViewList.Columns["InvoicesImage"].HeaderText = "单据附件";
            this.dataGridViewList.Columns["Status"].Visible = false;
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";
            this.dataGridViewList.Columns["InvoicesImage"].Visible = false;
            this.dataGridViewList.Columns["ReviewUserName"].Visible = false;
            this.dataGridViewList.Columns["SupplierName"].DisplayIndex = 3;

            SetToolButtonStatus(formStatus);

            //设置明细表 combobox 数据源
            GoodsCode.DataSource = _w_GoodsServices.Query();
            GoodsCode.ValueMember = "GoodsCode";
            GoodsCode.DisplayMember = "GoodsCode";
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_BuyReturn;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();           
            this.BuyReturnCode.Text = model.BuyReturnCode;  
            this.BuyCode.Text = model.BuyCode;  
            this.SupplierName.Text = model.SupplierName;
            this.SupplierCode.Text = model.SupplierCode;
            this.InvoicesDate.Text = model.InvoicesDate.ToString();  
            this.MakeUserName.Text = model.MakeUserName;  
            this.ReviewUserName.Text = model.ReviewUserName;  
            this.InvoicesImage.Text = model.InvoicesImage;  
            //this..Text = model.Status.ToString();
            this.Status.Text = model.Status;
            this.Remark.Text = model.Remark;
            SetDataToDetail(GetDetailData(model.BuyReturnCode));
            //设置界面统计数据
            SetFormTotalData();
            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
            SupplierName.ReadOnly = true;
            MakeUserName.ReadOnly = true;
            ReviewUserName.ReadOnly = true;
            MakeUserName.Text = AppInfo.User.Fullname;
            dataGridViewDetail.Rows.Clear();
            SetDataGridViewReadonlyStatus();
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            if (CheckData(BuyReturnCode.Text))
            {
                base.EditFunction(sender, e);
                SetToolButtonStatus(formStatus);
                SetDataGridViewReadonlyStatus();
            }
        }

        private void SetDataGridViewReadonlyStatus()
        {
            dataGridViewDetail.ReadOnly = false;
            string[] cols = { "ReturnNumber", "UnitPrice", "DiscountPrice", "TaxRate", "RemarkD" };
            foreach (DataGridViewColumn column in dataGridViewDetail.Columns)
            {
                if (!cols.Contains(column.Name))
                {
                    column.ReadOnly = true;
                }
            }
        }
        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_BuyReturn model = new w_BuyReturn();
                    // TODO获取界面的数据
                    if (string.IsNullOrEmpty(this.BuyReturnCode.Text))
                    {
                        model.BuyReturnCode = $"CGTH{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                    }
                    else
                    {
                        model.BuyReturnCode = this.BuyCode.Text;
                    }
                    model.BuyCode=this.BuyCode.Text;
                    model.SupplierCode=this.SupplierCode.Text;
                    model.SupplierName = this.SupplierName.Text;
                    model.InvoicesDate=this.InvoicesDate.Text.ToDateTime();
                    model.MakeUserName=this.MakeUserName.Text;
                    model.ReviewUserName=this.ReviewUserName.Text;
                    model.InvoicesImage=this.InvoicesImage.Text;
                    model.Status="1";
                    model.Remark=this.Remark.Text;
                    if (_w_BuyReturnServices.Exists(p => p.BuyReturnCode.Equals(model.BuyReturnCode)))
                    {
                        MessageBox.Show("退货货单号已存在！请稍后在保存！", "提示");
                        return;
                    }

                    #region 明细表

                    List<w_BuyReturnDetail> dataDetails = new List<w_BuyReturnDetail>();
                    foreach (DataGridViewRow row in dataGridViewDetail.Rows)
                    {
                        if (!row.IsNewRow)
                        {
                            w_BuyReturnDetail detail = new w_BuyReturnDetail();
                            detail.ReturnNumber = row.Cells["ReturnNumber"].FormattedValue.ToDecimal();
                            detail.GoodsName = row.Cells["GoodsName"].FormattedValue.ToString();
                            //detail.Warehouse = row.Cells["Warehouse"].Value.ToInt32();
                            detail.GoodsCode = row.Cells["GoodsCode"].FormattedValue.ToString();
                            detail.GoodsSpec = row.Cells["GoodsSpec"].FormattedValue.ToString();
                            detail.GoodsUnit = row.Cells["GoodsUnit"].FormattedValue.ToString();
                            detail.BuyCode = row.Cells["BuyCodeD"].FormattedValue.ToString();
                            detail.BuyReturnCode = row.Cells["BuyReturnCodeD"].FormattedValue.ToString();
                            detail.UnitPrice = row.Cells["UnitPrice"].FormattedValue.ToDecimal();
                            detail.DiscountPrice = row.Cells["DiscountPrice"].FormattedValue.ToDecimal();
                            detail.TotalPrice = row.Cells["TotalPrice"].FormattedValue.ToDecimal();
                            detail.TaxRate = row.Cells["TaxRate"].FormattedValue.ToDecimal();
                            detail.TaxUnitPrice = row.Cells["TaxUnitPrice"].FormattedValue.ToDecimal();
                            detail.TaxPrice = row.Cells["TaxPrice"].FormattedValue.ToDecimal();
                            detail.TotalTaxPrice = row.Cells["TotalTaxPrice"].FormattedValue.ToDecimal();
                            detail.Remark = row.Cells["RemarkD"].FormattedValue.ToString();
                            dataDetails.Add(detail);
                        }
                    }

                    int id = _w_BuyReturnServices.AddBuyReturnInfo(model, dataDetails);
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                        txtID.Text = id.ToString();
                        BuyReturnCode.Text = model.BuyReturnCode;
                        SetDataToDetail(GetDetailData(model.BuyReturnCode));
                    }
                    #endregion
                    
                }
                // 修改
                else
                {
                    w_BuyReturn model = _w_BuyReturnServices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.BuyCode = this.BuyCode.Text;
                    model.SupplierCode = this.SupplierCode.Text;
                    model.SupplierName = this.SupplierName.Text;
                    model.InvoicesDate = this.InvoicesDate.Text.ToDateTime();
                    model.MakeUserName = this.MakeUserName.Text;
                    model.ReviewUserName = this.ReviewUserName.Text;
                    model.InvoicesImage = this.InvoicesImage.Text;
                    model.Status = "01";
                    model.Remark = this.Remark.Text;

                    #region 明细表

                    List<w_BuyReturnDetail> dataDetails = new List<w_BuyReturnDetail>();
                    foreach (DataGridViewRow row in dataGridViewDetail.Rows)
                    {
                        if (!row.IsNewRow)
                        {
                            w_BuyReturnDetail detail = new w_BuyReturnDetail();
                            detail.ReturnNumber = row.Cells["ReturnNumber"].FormattedValue.ToDecimal();
                            detail.GoodsName = row.Cells["GoodsName"].FormattedValue.ToString();
                            detail.BuyReturnDetailCode = row.Cells["BuyReturnDetailCode"].FormattedValue.ToString();
                            detail.GoodsCode = row.Cells["GoodsCode"].FormattedValue.ToString();
                            detail.GoodsSpec = row.Cells["GoodsSpec"].FormattedValue.ToString();
                            detail.GoodsUnit = row.Cells["GoodsUnit"].FormattedValue.ToString();
                            detail.BuyCode = row.Cells["BuyCodeD"].FormattedValue.ToString();
                            detail.BuyReturnCode = row.Cells["BuyReturnCodeD"].FormattedValue.ToString();
                            detail.UnitPrice = row.Cells["UnitPrice"].FormattedValue.ToDecimal();
                            detail.DiscountPrice = row.Cells["DiscountPrice"].FormattedValue.ToDecimal();
                            detail.TotalPrice = row.Cells["TotalPrice"].FormattedValue.ToDecimal();
                            detail.TaxRate = row.Cells["TaxRate"].FormattedValue.ToDecimal();
                            detail.TaxUnitPrice = row.Cells["TaxUnitPrice"].FormattedValue.ToDecimal();
                            detail.TaxPrice = row.Cells["TaxPrice"].FormattedValue.ToDecimal();
                            detail.TotalTaxPrice = row.Cells["TotalTaxPrice"].FormattedValue.ToDecimal();
                            detail.Remark = row.Cells["RemarkD"].FormattedValue.ToString();
                            dataDetails.Add(detail);
                        }
                    }

                    if (_w_BuyReturnServices.UpdateBuyReturnInfo(model, dataDetails))
                    {
                        MessageBox.Show("保存成功！", "提示");
                        SetDataToDetail(GetDetailData(model.BuyReturnCode));
                    }
                    #endregion
                  
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled= !isAdd;
            SetDataToDetail(GetDetailData(BuyReturnCode.Text));
            SetFormTotalData();
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            if (CheckData(BuyReturnCode.Text))
            {
                base.DelFunction(sender, e);
                var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    _w_BuyReturnServices.DeleteById(Int32.Parse(txtID.Text));
                    _w_BuyReturnDetailServices.Delete(p => p.BuyReturnCode == BuyReturnCode.Text);
                    formStatus = FormStatus.Del;
                    SetToolButtonStatus(formStatus);
                    //列表重新赋值
                    dataGridViewDetail.Rows.Clear();
                    this.dataGridViewList.DataSource = GetData();
                    InvoicesImage.Tag = string.Empty;
                    InvoicesImage.Image = null;
                }
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        dataGridViewDetail.ReadOnly = false;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            if (string.IsNullOrEmpty(BuyCode.Text))
            {
                MessageBox.Show("关联的采购单号不能为空！");
                return false;
            }
            if (string.IsNullOrEmpty(SupplierName.Text))
            {
                MessageBox.Show("供应商不能为空！");
                return false;
            }
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_BuyReturn> GetData()
        {
            List<w_BuyReturn> data = new List<w_BuyReturn>();
            data = _w_BuyReturnServices.Query();
            return data;
        }

        private void BuyCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!string.IsNullOrEmpty(BuyCode.Text.Trim()))
                {
                    var data = _w_BuyServices.QueryByClause(p => p.BuyCode.Equals(BuyCode.Text.Trim()));
                    if (data != null)
                    {
                        SupplierName.Text = data.SupplierName;
                        SupplierCode.Text = data.SupplierCode;
                        //明细带出数据

                        List<w_BuyDetail> buyDetails=new List<w_BuyDetail>();
                        buyDetails = _w_BuyDetailServices.QueryListByClause(p => p.BuyCode == data.BuyCode);
                        SetDataToDetail(buyDetails);
                    }
                    else
                    {
                        MessageBox.Show("采购单号不存在！");
                    }
                }
            }
        }

        /// <summary>
        /// 明细表 设置数据 --采购单号带出
        /// </summary>
        /// <param name="details"></param>
        private void SetDataToDetail(List<w_BuyDetail> details)
        {
            dataGridViewDetail.Rows.Clear();
            details.ForEach(p =>
            {
                dataGridViewDetail.Rows.AddCopy(dataGridViewDetail.Rows.Count - 1);
                DataGridViewRow row = dataGridViewDetail.Rows[dataGridViewDetail.Rows.Count - 2];
                row.Cells["BuyCodeD"].Value = p.BuyCode;
                row.Cells["GoodsName"].Value = p.GoodsName;
                row.Cells["GoodsCode"].Value = p.GoodsCode;
                row.Cells["ReturnNumber"].Value = 0;
                row.Cells["GoodsSpec"].Value = p.GoodsSpec;
                row.Cells["GoodsUnit"].Value = p.GoodsUnit;
                row.Cells["UnitPrice"].Value = p.UnitPrice;
                row.Cells["DiscountPrice"].Value = 0;
                row.Cells["TotalPrice"].Value = 0;
                row.Cells["TaxRate"].Value = p.TaxRate;
                row.Cells["TaxUnitPrice"].Value = 0;
                row.Cells["TaxPrice"].Value = 0;
                row.Cells["TotalTaxPrice"].Value = 0;
                row.Cells["RemarkD"].Value = p.Remark;
                row.Cells["TotalTaxPrice"].Value = 0;
                //row.Cells["BuyID"].Value = p.BuyID;
                //row.Cells["BuyCodeD"].Value = p.BuyCode;
                //row.Cells["BuyDetailCode"].Value = p.BuyDetailCode;
            });
        }
        /// <summary>
        /// 明细表 设置数据 --主表带出
        /// </summary>
        /// <param name="details"></param>
        private void SetDataToDetail(List<w_BuyReturnDetail> details)
        {
            dataGridViewDetail.Rows.Clear();
            details.ForEach(p =>
            {
                dataGridViewDetail.Rows.AddCopy(dataGridViewDetail.Rows.Count - 1);
                DataGridViewRow row = dataGridViewDetail.Rows[dataGridViewDetail.Rows.Count - 2];
                row.Cells["BuyCodeD"].Value = p.BuyCode;
                row.Cells["GoodsName"].Value = p.GoodsName;
                row.Cells["GoodsCode"].Value = p.GoodsCode;
                row.Cells["ReturnNumber"].Value = p.ReturnNumber;
                row.Cells["GoodsSpec"].Value = p.GoodsSpec;
                row.Cells["GoodsUnit"].Value = p.GoodsUnit;
                row.Cells["UnitPrice"].Value = p.UnitPrice;
                row.Cells["DiscountPrice"].Value = p.DiscountPrice;
                row.Cells["TotalPrice"].Value = p.TotalPrice;
                row.Cells["TaxRate"].Value = p.TaxRate;
                row.Cells["TaxUnitPrice"].Value = p.TaxUnitPrice;
                row.Cells["TaxPrice"].Value = p.TaxPrice;
                row.Cells["TotalTaxPrice"].Value = p.TotalTaxPrice;
                row.Cells["RemarkD"].Value = p.Remark;
                row.Cells["BuyReturnCodeD"].Value = p.BuyReturnCode;
                row.Cells["BuyCodeD"].Value = p.BuyCode;
                row.Cells["BuyReturnDetailCode"].Value = p.BuyReturnDetailCode;
            });
        }
        private void InvoicesImage_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.jpg, *.png, *.gif, *.bmp)|*.jpg; *.png; *.gif; *.bmp";
            openFileDialog.Multiselect = false;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // 检查图像大小
                FileInfo fileInfo = new FileInfo(openFileDialog.FileName);
                long fileSize = fileInfo.Length;

                if (fileSize > 1024 * 1024) // 1MB = 1024 * 1024 bytes
                {
                    MessageBox.Show("图片大小不能超过1MB");
                    return;
                }
                Image image = Image.FromFile(openFileDialog.FileName);
                InvoicesImage.Image = image;
                var str = image.ImageToBase64();
                InvoicesImage.Tag = str;
            }
        }

        private void dataGridViewDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                //数量
                if (dataGridViewDetail.Columns[e.ColumnIndex].Name == "ReturnNumber" || dataGridViewDetail.Columns[e.ColumnIndex].Name == "UnitPrice" || dataGridViewDetail.Columns[e.ColumnIndex].Name == "DiscountPrice" || dataGridViewDetail.Columns[e.ColumnIndex].Name == "TaxRate")
                {
                    decimal value = dataGridViewDetail.Rows[e.RowIndex].Cells["ReturnNumber"].Value == null ? 0 : dataGridViewDetail.Rows[e.RowIndex].Cells["ReturnNumber"].Value.ToDecimal();
                    //购货单价
                    decimal unitPrice = dataGridViewDetail.Rows[e.RowIndex].Cells["UnitPrice"].Value == null ? 0 : dataGridViewDetail.Rows[e.RowIndex].Cells["UnitPrice"].Value.ToDecimal();
                    //折扣金额
                    decimal discountPrice = dataGridViewDetail.Rows[e.RowIndex].Cells["DiscountPrice"].Value == null ? 0 : dataGridViewDetail.Rows[e.RowIndex].Cells["DiscountPrice"].Value.ToDecimal();
                    //税率
                    decimal taxRate = dataGridViewDetail.Rows[e.RowIndex].Cells["TaxRate"].Value == null ? 0 : dataGridViewDetail.Rows[e.RowIndex].Cells["TaxRate"].Value.ToDecimal()/100;

                    decimal jine= value * unitPrice - discountPrice;
                    //金额
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TotalPrice"].Value = jine;

                    decimal danjia= unitPrice * (1 + taxRate);

                    //含税单价
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TaxUnitPrice"].Value = danjia;

                    //税额
                    decimal shuie = unitPrice * (taxRate) * value - discountPrice * taxRate;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TaxPrice"].Value = shuie;

                    //含税金额
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TotalTaxPrice"].Value =
                        shuie+jine;
                    SetFormTotalData();
                }
            }
        }
        private void SetFormTotalData()
        {
            decimal total = 0;
            foreach (DataGridViewRow row in dataGridViewDetail.Rows)
            {
                if (!row.IsNewRow)
                {
                    total += row.Cells["TotalTaxPrice"].Value.ToDecimal();
                }
            }
            ToltalPrice.Text = total.ToString();
        }

        private void InvoicesImage_DoubleClick(object sender, EventArgs e)
        {
            if (InvoicesImage.Image == null) return;
            // 获取PictureBox控件中的图片
            byte[] imageBytes = Convert.FromBase64String(InvoicesImage.Tag.ToString());
            // 保存图片到临时文件
            string tempFilePath = Application.StartupPath + $"goods.{InvoicesImage.Image.RawFormat}";
            File.WriteAllBytes(tempFilePath, imageBytes);
            //建立新的系统进程    
            System.Diagnostics.Process process = new System.Diagnostics.Process();

            //设置图片的真实路径和文件名    
            process.StartInfo.FileName = tempFilePath;

            //设置进程运行参数，这里以最大化窗口方法显示图片。    
            process.StartInfo.Arguments = "rundl132.exe C://WINDOWS//system32//shimgvw.dll,ImageView_Fullscreen";

            //此项为是否使用Shell执行程序，因系统默认为true，此项也可不设，但若设置必须为true    
            process.StartInfo.UseShellExecute = true;

            //此处可以更改进程所打开窗体的显示样式，可以不设    
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            process.Start();
            process.Close();
            // 删除临时文件
            //File.Delete(tempFilePath);
        }
        /// <summary>
        /// 获取明细数据
        /// </summary>
        /// <returns></returns>
        private List<w_BuyReturnDetail> GetDetailData(string code)
        {
            List<w_BuyReturnDetail> data = new List<w_BuyReturnDetail>();
            data = _w_BuyReturnDetailServices.QueryListByClause(p => p.BuyReturnCode == code);
            return data;
        }
        /// <summary>
        /// 数据校验
        /// </summary>
        /// <returns></returns>
        private bool CheckData(string code)
        {
            //是否已出库
            if (_w_buyReturnOutWarehouseDetailServices.Exists(p => p.BuyReturnCode == code))
            {
                MessageBox.Show("该采购退货单已出库，不能操作！", "提示");
                return false;
            }
            return true;
        }
    }
}
