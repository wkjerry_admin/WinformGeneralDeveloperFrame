namespace WinformDevFramework
{
    partial class Frmw_BuyReturnOutWarehouse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBuyReturnCode = new System.Windows.Forms.Label();
            this.BuyReturnCode = new System.Windows.Forms.TextBox();
            this.lblBuyCode = new System.Windows.Forms.Label();
            this.BuyCode = new System.Windows.Forms.TextBox();
            this.lblBuyReturnOutWarehouseCode = new System.Windows.Forms.Label();
            this.BuyReturnOutWarehouseCode = new System.Windows.Forms.TextBox();
            this.lblSupplierCode = new System.Windows.Forms.Label();
            this.SupplierCode = new System.Windows.Forms.TextBox();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.SupplierName = new System.Windows.Forms.TextBox();
            this.lblInvoicesDate = new System.Windows.Forms.Label();
            this.InvoicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblInvoicesImage = new System.Windows.Forms.Label();
            this.InvoicesImage = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyReturnCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyReturnOutWarehouseCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyReturnOutWarehouseDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Warehouse = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(2);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.InvoicesImage);
            this.groupBox1.Controls.Add(this.lblInvoicesImage);
            this.groupBox1.Controls.Add(this.InvoicesDate);
            this.groupBox1.Controls.Add(this.lblInvoicesDate);
            this.groupBox1.Controls.Add(this.SupplierName);
            this.groupBox1.Controls.Add(this.lblSupplierName);
            this.groupBox1.Controls.Add(this.SupplierCode);
            this.groupBox1.Controls.Add(this.lblSupplierCode);
            this.groupBox1.Controls.Add(this.BuyReturnOutWarehouseCode);
            this.groupBox1.Controls.Add(this.lblBuyReturnOutWarehouseCode);
            this.groupBox1.Controls.Add(this.BuyCode);
            this.groupBox1.Controls.Add(this.lblBuyCode);
            this.groupBox1.Controls.Add(this.BuyReturnCode);
            this.groupBox1.Controls.Add(this.lblBuyReturnCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(780, 379);
            this.groupBox1.Controls.SetChildIndex(this.lblBuyReturnCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.BuyReturnCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblBuyCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.BuyCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblBuyReturnOutWarehouseCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.BuyReturnOutWarehouseCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.Status, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(2);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Size = new System.Drawing.Size(53, 23);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(59, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(116, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Size = new System.Drawing.Size(53, 23);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(173, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCanel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(230, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnDel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(287, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetPW.Size = new System.Drawing.Size(53, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(401, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(344, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Size = new System.Drawing.Size(53, 23);
            // 
            // lblBuyReturnCode
            // 
            this.lblBuyReturnCode.Location = new System.Drawing.Point(10, 10);
            this.lblBuyReturnCode.Name = "lblBuyReturnCode";
            this.lblBuyReturnCode.Size = new System.Drawing.Size(85, 23);
            this.lblBuyReturnCode.TabIndex = 2;
            this.lblBuyReturnCode.Text = "采购退货单号";
            this.lblBuyReturnCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyReturnCode
            // 
            this.BuyReturnCode.Location = new System.Drawing.Point(100, 10);
            this.BuyReturnCode.Name = "BuyReturnCode";
            this.BuyReturnCode.Size = new System.Drawing.Size(130, 23);
            this.BuyReturnCode.TabIndex = 1;
            // 
            // lblBuyCode
            // 
            this.lblBuyCode.Location = new System.Drawing.Point(230, 10);
            this.lblBuyCode.Name = "lblBuyCode";
            this.lblBuyCode.Size = new System.Drawing.Size(85, 23);
            this.lblBuyCode.TabIndex = 4;
            this.lblBuyCode.Text = "采购单号";
            this.lblBuyCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyCode
            // 
            this.BuyCode.Location = new System.Drawing.Point(320, 10);
            this.BuyCode.Name = "BuyCode";
            this.BuyCode.Size = new System.Drawing.Size(130, 23);
            this.BuyCode.TabIndex = 3;
            // 
            // lblBuyReturnOutWarehouseCode
            // 
            this.lblBuyReturnOutWarehouseCode.Location = new System.Drawing.Point(450, 10);
            this.lblBuyReturnOutWarehouseCode.Name = "lblBuyReturnOutWarehouseCode";
            this.lblBuyReturnOutWarehouseCode.Size = new System.Drawing.Size(112, 23);
            this.lblBuyReturnOutWarehouseCode.TabIndex = 6;
            this.lblBuyReturnOutWarehouseCode.Text = "采购退货出库单号";
            this.lblBuyReturnOutWarehouseCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyReturnOutWarehouseCode
            // 
            this.BuyReturnOutWarehouseCode.Location = new System.Drawing.Point(568, 10);
            this.BuyReturnOutWarehouseCode.Name = "BuyReturnOutWarehouseCode";
            this.BuyReturnOutWarehouseCode.PlaceholderText = "为空自动生成";
            this.BuyReturnOutWarehouseCode.Size = new System.Drawing.Size(130, 23);
            this.BuyReturnOutWarehouseCode.TabIndex = 5;
            // 
            // lblSupplierCode
            // 
            this.lblSupplierCode.Location = new System.Drawing.Point(10, 45);
            this.lblSupplierCode.Name = "lblSupplierCode";
            this.lblSupplierCode.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierCode.TabIndex = 8;
            this.lblSupplierCode.Text = "供应商编码";
            this.lblSupplierCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SupplierCode
            // 
            this.SupplierCode.Location = new System.Drawing.Point(100, 45);
            this.SupplierCode.Name = "SupplierCode";
            this.SupplierCode.Size = new System.Drawing.Size(130, 23);
            this.SupplierCode.TabIndex = 7;
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.Location = new System.Drawing.Point(230, 45);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierName.TabIndex = 10;
            this.lblSupplierName.Text = "供应商名称";
            this.lblSupplierName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SupplierName
            // 
            this.SupplierName.Location = new System.Drawing.Point(320, 45);
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.Size = new System.Drawing.Size(130, 23);
            this.SupplierName.TabIndex = 9;
            // 
            // lblInvoicesDate
            // 
            this.lblInvoicesDate.Location = new System.Drawing.Point(450, 45);
            this.lblInvoicesDate.Name = "lblInvoicesDate";
            this.lblInvoicesDate.Size = new System.Drawing.Size(112, 23);
            this.lblInvoicesDate.TabIndex = 12;
            this.lblInvoicesDate.Text = "单据日期";
            this.lblInvoicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvoicesDate
            // 
            this.InvoicesDate.Location = new System.Drawing.Point(568, 45);
            this.InvoicesDate.Name = "InvoicesDate";
            this.InvoicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvoicesDate.TabIndex = 11;
            // 
            // lblInvoicesImage
            // 
            this.lblInvoicesImage.Location = new System.Drawing.Point(10, 80);
            this.lblInvoicesImage.Name = "lblInvoicesImage";
            this.lblInvoicesImage.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesImage.TabIndex = 14;
            this.lblInvoicesImage.Text = "单据附件";
            this.lblInvoicesImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblInvoicesImage.Visible = false;
            // 
            // InvoicesImage
            // 
            this.InvoicesImage.Location = new System.Drawing.Point(100, 80);
            this.InvoicesImage.Name = "InvoicesImage";
            this.InvoicesImage.Size = new System.Drawing.Size(130, 23);
            this.InvoicesImage.TabIndex = 13;
            this.InvoicesImage.Visible = false;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 83);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 16;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 83);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 15;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(450, 80);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(112, 23);
            this.lblStatus.TabIndex = 18;
            this.lblStatus.Text = "单据状态";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblStatus.Visible = false;
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(568, 80);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(130, 23);
            this.Status.TabIndex = 17;
            this.Status.Visible = false;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(3, 109);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(773, 266);
            this.tabControl2.TabIndex = 19;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(765, 236);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "退货明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.BuyCodeD,
            this.BuyReturnCodeD,
            this.BuyReturnOutWarehouseCodeD,
            this.BuyReturnOutWarehouseDetailCode,
            this.Warehouse,
            this.GoodsCode,
            this.GoodsName,
            this.GoodsSpec,
            this.GoodsUnit,
            this.Number,
            this.RemarkD});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowHeadersWidth = 72;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(759, 230);
            this.dataGridViewDetail.TabIndex = 1;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 9;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // BuyCodeD
            // 
            this.BuyCodeD.DataPropertyName = "BuyCode";
            this.BuyCodeD.HeaderText = "采购单号";
            this.BuyCodeD.MinimumWidth = 9;
            this.BuyCodeD.Name = "BuyCodeD";
            this.BuyCodeD.ReadOnly = true;
            this.BuyCodeD.Visible = false;
            // 
            // BuyReturnCodeD
            // 
            this.BuyReturnCodeD.DataPropertyName = "BuyReturnCode";
            this.BuyReturnCodeD.HeaderText = "采购退货单号";
            this.BuyReturnCodeD.MinimumWidth = 9;
            this.BuyReturnCodeD.Name = "BuyReturnCodeD";
            this.BuyReturnCodeD.ReadOnly = true;
            this.BuyReturnCodeD.Visible = false;
            // 
            // BuyReturnOutWarehouseCodeD
            // 
            this.BuyReturnOutWarehouseCodeD.DataPropertyName = "BuyReturnOutWarehouseCode";
            this.BuyReturnOutWarehouseCodeD.HeaderText = "采购退货出库单号";
            this.BuyReturnOutWarehouseCodeD.MinimumWidth = 9;
            this.BuyReturnOutWarehouseCodeD.Name = "BuyReturnOutWarehouseCodeD";
            this.BuyReturnOutWarehouseCodeD.ReadOnly = true;
            this.BuyReturnOutWarehouseCodeD.Visible = false;
            // 
            // BuyReturnOutWarehouseDetailCode
            // 
            this.BuyReturnOutWarehouseDetailCode.DataPropertyName = "BuyReturnOutWarehouseDetailCode";
            this.BuyReturnOutWarehouseDetailCode.HeaderText = "采购退货出库明细单号";
            this.BuyReturnOutWarehouseDetailCode.MinimumWidth = 9;
            this.BuyReturnOutWarehouseDetailCode.Name = "BuyReturnOutWarehouseDetailCode";
            this.BuyReturnOutWarehouseDetailCode.ReadOnly = true;
            this.BuyReturnOutWarehouseDetailCode.Visible = false;
            // 
            // Warehouse
            // 
            this.Warehouse.HeaderText = "仓库";
            this.Warehouse.MinimumWidth = 9;
            this.Warehouse.Name = "Warehouse";
            // 
            // GoodsCode
            // 
            this.GoodsCode.DataPropertyName = "GoodsCode";
            this.GoodsCode.HeaderText = "商品编码";
            this.GoodsCode.MinimumWidth = 9;
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.ReadOnly = true;
            this.GoodsCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GoodsCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GoodsName
            // 
            this.GoodsName.DataPropertyName = "GoodsName";
            this.GoodsName.HeaderText = "商品名称";
            this.GoodsName.MinimumWidth = 9;
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.ReadOnly = true;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.DataPropertyName = "GoodsSpec";
            this.GoodsSpec.HeaderText = "商品规格";
            this.GoodsSpec.MinimumWidth = 9;
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.ReadOnly = true;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.DataPropertyName = "GoodsUnit";
            this.GoodsUnit.HeaderText = "计量单位";
            this.GoodsUnit.MinimumWidth = 9;
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.ReadOnly = true;
            // 
            // Number
            // 
            this.Number.DataPropertyName = "Number";
            this.Number.HeaderText = "数量";
            this.Number.MinimumWidth = 9;
            this.Number.Name = "Number";
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 9;
            this.RemarkD.Name = "RemarkD";
            // 
            // Frmw_BuyReturnOutWarehouse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_BuyReturnOutWarehouse";
            this.Text = "Frmw_BuyReturnOutWarehouse";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox BuyReturnCode;
        private Label lblBuyReturnCode;
        private TextBox BuyCode;
        private Label lblBuyCode;
        private TextBox BuyReturnOutWarehouseCode;
        private Label lblBuyReturnOutWarehouseCode;
        private TextBox SupplierCode;
        private Label lblSupplierCode;
        private TextBox SupplierName;
        private Label lblSupplierName;
        private DateTimePicker InvoicesDate;
        private Label lblInvoicesDate;
        private TextBox InvoicesImage;
        private Label lblInvoicesImage;
        private TextBox Remark;
        private Label lblRemark;
        private TextBox Status;
        private Label lblStatus;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn BuyCodeD;
        private DataGridViewTextBoxColumn BuyReturnCodeD;
        private DataGridViewTextBoxColumn BuyReturnOutWarehouseCodeD;
        private DataGridViewTextBoxColumn BuyReturnOutWarehouseDetailCode;
        private DataGridViewComboBoxColumn Warehouse;
        private DataGridViewTextBoxColumn GoodsCode;
        private DataGridViewTextBoxColumn GoodsName;
        private DataGridViewTextBoxColumn GoodsSpec;
        private DataGridViewTextBoxColumn GoodsUnit;
        private DataGridViewTextBoxColumn Number;
        private DataGridViewTextBoxColumn RemarkD;
    }
}