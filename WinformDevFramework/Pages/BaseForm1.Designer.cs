﻿namespace WinformDevFramework
{
    partial class BaseForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm1));
            this.palTools = new System.Windows.Forms.Panel();
            this.flowLayoutPanelTools = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCanel = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnResetPW = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabList = new System.Windows.Forms.TabPage();
            this.dataGridViewList = new System.Windows.Forms.DataGridView();
            this.tabDataEdit = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.palTools.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).BeginInit();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.palTools.Controls.Add(this.flowLayoutPanelTools);
            this.palTools.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTools.Location = new System.Drawing.Point(0, 0);
            this.palTools.Name = "palTools";
            this.palTools.Size = new System.Drawing.Size(800, 31);
            this.palTools.TabIndex = 0;
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Controls.Add(this.btnAdd);
            this.flowLayoutPanelTools.Controls.Add(this.btnEdit);
            this.flowLayoutPanelTools.Controls.Add(this.btnSave);
            this.flowLayoutPanelTools.Controls.Add(this.btnCanel);
            this.flowLayoutPanelTools.Controls.Add(this.btnDel);
            this.flowLayoutPanelTools.Controls.Add(this.btnResetPW);
            this.flowLayoutPanelTools.Controls.Add(this.btnSearch);
            this.flowLayoutPanelTools.Controls.Add(this.btnClose);
            this.flowLayoutPanelTools.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelTools.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelTools.Name = "flowLayoutPanelTools";
            this.flowLayoutPanelTools.Size = new System.Drawing.Size(796, 27);
            this.flowLayoutPanelTools.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(3, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(55, 25);
            this.btnAdd.TabIndex = 13;
            this.btnAdd.Text = "新增";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(64, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(55, 25);
            this.btnEdit.TabIndex = 14;
            this.btnEdit.Text = "编辑";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(125, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(55, 25);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "保存";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCanel
            // 
            this.btnCanel.Image = ((System.Drawing.Image)(resources.GetObject("btnCanel.Image")));
            this.btnCanel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCanel.Location = new System.Drawing.Point(186, 3);
            this.btnCanel.Name = "btnCanel";
            this.btnCanel.Size = new System.Drawing.Size(55, 25);
            this.btnCanel.TabIndex = 17;
            this.btnCanel.Text = "撤销";
            this.btnCanel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCanel.UseVisualStyleBackColor = true;
            this.btnCanel.Click += new System.EventHandler(this.btnCanel_Click);
            // 
            // btnDel
            // 
            this.btnDel.Image = ((System.Drawing.Image)(resources.GetObject("btnDel.Image")));
            this.btnDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDel.Location = new System.Drawing.Point(247, 3);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(55, 25);
            this.btnDel.TabIndex = 16;
            this.btnDel.Text = "删除";
            this.btnDel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Image = ((System.Drawing.Image)(resources.GetObject("btnResetPW.Image")));
            this.btnResetPW.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnResetPW.Location = new System.Drawing.Point(308, 3);
            this.btnResetPW.Name = "btnResetPW";
            this.btnResetPW.Size = new System.Drawing.Size(80, 23);
            this.btnResetPW.TabIndex = 19;
            this.btnResetPW.Text = "重置密码";
            this.btnResetPW.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnResetPW.UseVisualStyleBackColor = true;
            this.btnResetPW.Click += new System.EventHandler(this.btnResetPW_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(394, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(55, 25);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "搜索";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(455, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(55, 25);
            this.btnClose.TabIndex = 18;
            this.btnClose.Text = "关闭";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabList);
            this.tabControl1.Controls.Add(this.tabDataEdit);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 31);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 419);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabList
            // 
            this.tabList.Controls.Add(this.dataGridViewList);
            this.tabList.Location = new System.Drawing.Point(4, 26);
            this.tabList.Name = "tabList";
            this.tabList.Padding = new System.Windows.Forms.Padding(3);
            this.tabList.Size = new System.Drawing.Size(792, 389);
            this.tabList.TabIndex = 0;
            this.tabList.Text = "数据列表";
            this.tabList.UseVisualStyleBackColor = true;
            // 
            // dataGridViewList
            // 
            this.dataGridViewList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewList.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewList.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewList.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewList.Name = "dataGridViewList";
            this.dataGridViewList.RowHeadersVisible = false;
            this.dataGridViewList.RowTemplate.Height = 25;
            this.dataGridViewList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewList.Size = new System.Drawing.Size(786, 383);
            this.dataGridViewList.TabIndex = 0;
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Controls.Add(this.groupBox1);
            this.tabDataEdit.Location = new System.Drawing.Point(4, 26);
            this.tabDataEdit.Name = "tabDataEdit";
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(3);
            this.tabDataEdit.Size = new System.Drawing.Size(792, 389);
            this.tabDataEdit.TabIndex = 1;
            this.tabDataEdit.Text = "数据编辑";
            this.tabDataEdit.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(786, 383);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(370, 51);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 23);
            this.txtID.TabIndex = 0;
            this.txtID.Visible = false;
            // 
            // BaseForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.palTools);
            this.Name = "BaseForm1";
            this.Text = "BaseForm1";
            this.Load += new System.EventHandler(this.BaseForm1_Load);
            this.palTools.ResumeLayout(false);
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).EndInit();
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected Panel palTools;
        protected TabControl tabControl1;
        protected TabPage tabList;
        protected TabPage tabDataEdit;
        protected GroupBox groupBox1;
        protected FlowLayoutPanel flowLayoutPanelTools;
        protected DataGridView dataGridViewList;
        protected TextBox txtID;
        protected Button btnAdd;
        protected Button btnEdit;
        protected Button btnSave;
        protected Button btnCanel;
        protected Button btnDel;
        protected Button btnResetPW;
        protected Button btnClose;
        protected Button btnSearch;
    }
}