﻿using Autofac;
using System.Data;
using Tulpep.NotificationWindow;
using WinformDevFramework;
using WinformDevFramework.IServices;
using WinformDevFramework.IServices.System;
using WinformDevFramework.Models;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WinformDevFarme
{
    public partial class FrmMain : Form
    {
        private Point mPoint;
        private ISysMenuServices _sysMenuServices;
        private IsysMessageServices _sysMessageServices;
        public FrmMain(ISysMenuServices sysMenuServices,IsysMessageServices sysMessageServices)
        {
            _sysMenuServices = sysMenuServices;
            _sysMessageServices = sysMessageServices;
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mPoint=new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(this.Location.X + e.X - mPoint.X, this.Location.Y + e.Y - mPoint.Y);
            }
        }

        private void InitData()
        {
            lblUsername.Text = AppInfo.User.Fullname;
            //初始化导航菜单

            var tmenus = AppInfo.UserMenus.Where(p=>p.MenuType.Equals("Menu")|| p.MenuType.Equals("Form"));
            ImageList imageList = new ImageList();
            treeView1.ImageList = imageList;
            List<TreeNode> nodes = new List<TreeNode>();
            foreach (var menu in tmenus)
            {
                TreeNode node = new TreeNode(menu.Title);
                node.Tag = menu;
                node.Name = menu.ID.ToString();
                if (File.Exists(Application.StartupPath + "\\" + menu.Icon))
                    imageList.Images.Add(menu.ID.ToString(), Image.FromFile(Application.StartupPath + "\\" + menu.Icon));
                node.ImageKey = menu.ID.ToString();
                node.SelectedImageKey = menu.ID.ToString();
                nodes.Add(node);
            }

            foreach (var treeNode in nodes)
            {
                var menu = (sysMenu)treeNode.Tag;
                treeNode.Nodes.AddRange(nodes.Where(p=> menu.ID== ((sysMenu)p.Tag).ParentID).ToArray());
            }

            treeView1.Nodes.Add(nodes.Where(p => ((sysMenu)p.Tag).ParentID == 0).FirstOrDefault());
            this.treeView1.NodeMouseDoubleClick += (o, e) =>
            {
                var menu = (sysMenu)e.Node.Tag;
                if (menu!=null && menu.MenuType.Equals("Form"))
                {
                    var childForm = AppInfo.Container.ResolveNamed<Form>(menu.URL);
                    childForm.TopLevel = false;
                    childForm.FormBorderStyle = FormBorderStyle.None;
                    childForm.Dock = DockStyle.Fill;
                    childForm.Tag=menu.ID;
                    if (tabControl1.TabPages[menu.URL] == null)
                    {
                        TabPage page = new TabPage(menu.Title);
                        page.Name = menu.URL;
                        page.Controls.Add(childForm);
                        tabControl1.TabPages.Add(page);
                        tabControl1.SelectedTab = page;
                        childForm.Show();
                    }
                    else
                    {
                        tabControl1.SelectedTab = tabControl1.TabPages[menu.URL];
                    }
 
                }
            };
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            InitData();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDatetime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            List<sysMessage> messages= _sysMessageServices.QueryListByClause(p => p.IsSend == false && p.ToUserID == AppInfo.User.ID);
            foreach (var message in messages)
            {
                // 创建一个PopupNotifier实例
                PopupNotifier popup = new PopupNotifier();
                popup.Size = new Size(300, 500);
                popup.TitleText = message.Title;
                popup.ContentText = message.Content;

                // 设置通知的样式和位置
                popup.AnimationDuration = 500;
                popup.Delay = 3000;
                popup.AnimationInterval = 20;
                popup.Popup();

                message.IsSend = true;
                message.SendTime=DateTime.Now;
                _sysMessageServices.Update(message);
            }
        }
    }
}
