using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_SettlementAccountServices : BaseServices<w_SettlementAccount>, Iw_SettlementAccountServices
    {
        private readonly Iw_SettlementAccountRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_SettlementAccountServices(IUnitOfWork unitOfWork, Iw_SettlementAccountRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}