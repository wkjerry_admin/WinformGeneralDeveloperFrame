using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_OtherOutWarehouseServices : BaseServices<w_OtherOutWarehouse>, Iw_OtherOutWarehouseServices
    {
        private readonly Iw_OtherOutWarehouseRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_OtherOutWarehouseServices(IUnitOfWork unitOfWork, Iw_OtherOutWarehouseRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddOtherOutWarehouseInfo(w_OtherOutWarehouse buy, List<w_OtherOutWarehouseDetail> detail)
        {
            return _dal.AddOtherOutWarehouseInfo(buy, detail);
        }

        public bool UpdateOtherOutWarehouseInfo(w_OtherOutWarehouse buy, List<w_OtherOutWarehouseDetail> detail)
        {
            return _dal.UpdateOtherOutWarehouseInfo(buy, detail);
        }

        public bool DeleteOtherOutWarehouseInfo(w_OtherOutWarehouse buy)
        {
            return _dal.DeleteOtherOutWarehouseInfo(buy);
        }
    }
}