using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_OtherInWarehouseServices : BaseServices<w_OtherInWarehouse>, Iw_OtherInWarehouseServices
    {
        private readonly Iw_OtherInWarehouseRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_OtherInWarehouseServices(IUnitOfWork unitOfWork, Iw_OtherInWarehouseRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddOtherInWarehouseInfo(w_OtherInWarehouse buy, List<w_OtherInWarehouseDetail> detail)
        {
            return _dal.AddOtherInWarehouseInfo(buy, detail);
        }

        public bool UpdateOtherInWarehouseInfo(w_OtherInWarehouse buy, List<w_OtherInWarehouseDetail> detail)
        {
            return _dal.UpdateOtherInWarehouseInfo(buy, detail);
        }

        public bool DeleteOtherInWarehouseInfo(w_OtherInWarehouse buy)
        {
            return _dal.DeleteOtherInWarehouseInfo(buy);
        }
    }
}