using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_OtherOutlayServices : BaseServices<w_OtherOutlay>, Iw_OtherOutlayServices
    {
        private readonly Iw_OtherOutlayRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_OtherOutlayServices(IUnitOfWork unitOfWork, Iw_OtherOutlayRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}