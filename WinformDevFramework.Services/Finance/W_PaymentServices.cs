using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_PaymentServices : BaseServices<w_Payment>, Iw_PaymentServices
    {
        private readonly Iw_PaymentRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_PaymentServices(IUnitOfWork unitOfWork, Iw_PaymentRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddPaymentInfo(w_Payment buy, List<w_PaymentDetail> detail)
        {
            return _dal.AddPaymentInfo(buy, detail);
        }

        public bool UpdatePaymentInfo(w_Payment buy, List<w_PaymentDetail> detail)
        {
            return _dal.UpdatePaymentInfo(buy, detail);
        }

        public bool DeletePaymentInfo(w_Payment buy)
        {
            return _dal.DeletePaymentInfo(buy);
        }
    }
}