using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_SaleDetailRepository : BaseRepository<w_SaleDetail>, Iw_SaleDetailRepository
    {
        public w_SaleDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}