using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_SaleReturnDetailRepository : BaseRepository<w_SaleReturnDetail>, Iw_SaleReturnDetailRepository
    {
        public w_SaleReturnDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}