using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_SaleOutWarehouseDetailRepository : BaseRepository<w_SaleOutWarehouseDetail>, Iw_SaleOutWarehouseDetailRepository
    {
        public w_SaleOutWarehouseDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}