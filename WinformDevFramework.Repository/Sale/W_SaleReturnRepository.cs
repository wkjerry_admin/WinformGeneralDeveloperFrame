using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar.Extensions;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_SaleReturnRepository : BaseRepository<w_SaleReturn>, Iw_SaleReturnRepository
    {
        private ISqlSugarClient _sqlSugarClient;
        public w_SaleReturnRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {
            _sqlSugarClient=sqlSugar;
        }

        public int AddSaleReturnInfo(w_SaleReturn buy, List<w_SaleReturnDetail> detail)
        {
            int id = 0;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                id = base.Insert(buy);
                int num = 1;
                detail.ForEach(p =>
                {
                    p.SaleCode = buy.SaleCode;
                    p.SaleReturnCode=buy.SaleReturnCode;
                    p.SaleReturnDetailCode = buy.SaleReturnCode + num.ToString("000");
                    num++;
                });
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return id;
        }

        public bool UpdateSaleReturnInfo(w_SaleReturn buy, List<w_SaleReturnDetail> detail)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.Update(buy);
                //删除明细数据
                DbBaseClient.Deleteable<w_SaleReturnDetail>(p => p.SaleReturnCode == buy.SaleReturnCode).ExecuteCommand();
                var d = detail.Where(p => !string.IsNullOrEmpty(p.SaleReturnDetailCode)).Select(p => p.SaleReturnDetailCode).ToList();
                int num = 0;
                d.ForEach(p =>
                {
                    var m = p.Substring(p.Length - 3, 3).ObjToInt();
                    if (num < m)
                    {
                        num = m;
                    }
                });
                detail.ForEach(p =>
                {
                    if (string.IsNullOrEmpty(p.SaleCode))
                    {
                        num++;
                        p.SaleCode = buy.SaleCode;
                        p.SaleReturnCode = buy.SaleReturnCode;
                        p.SaleReturnDetailCode = buy.SaleReturnCode + num.ToString("000");
                    }
                });
                //增加明细数据
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }
    }
}