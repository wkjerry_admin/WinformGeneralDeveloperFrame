using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar.Extensions;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
using System.Diagnostics;

namespace WinformDevFramework.Repository
{
    public class w_ReceivableRepository : BaseRepository<w_Receivable>, Iw_ReceivableRepository
    {
        private ISqlSugarClient _sqlSugarClient;
        public w_ReceivableRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {
            _sqlSugarClient= sqlSugar;
        }

        public int AddReceivableInfo(w_Receivable buy, List<w_ReceivableDetail> detail)
        {
            int id = 0;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                id = base.Insert(buy);
                int num = 1;
                //修改销售订单 金额信息
                detail.ForEach(p =>
                {
                    var buyInfo = _sqlSugarClient.Queryable<w_Sale>().Where(x => x.SaleCode == p.SaleCode).First();
                    if (buyInfo != null)
                    {
                        buyInfo.TotalPrice1 += p.Amount4;
                        buyInfo.TotalPrice2 -= p.Amount4;
                        _sqlSugarClient.Updateable<w_Sale>(buyInfo).ExecuteCommand();
                    }
                });
                decimal? price = 0;
                detail.ForEach(p =>
                {
                    p.ReceivableCode = buy.ReceivableCode;
                    p.ReceivableDetailCode = buy.ReceivableCode + num.ToString("000");
                    p.Amount2 = p.Amount4;
                    p.Amount3 = p.Amount1 - p.Amount4;
                    price += p.Amount4;
                    num++;
                });
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();
                //查找结算账户
                var settlementInfo = _sqlSugarClient.Queryable<w_SettlementAccount>()
                    .Where(p => p.Code == buy.SettlementCode).First();
                if (settlementInfo != null)
                {
                    settlementInfo.NowAmount += price;
                }
                _sqlSugarClient.Updateable(settlementInfo).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return id;
        }

        public bool UpdateReceivableInfo(w_Receivable buy, List<w_ReceivableDetail> detail)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.Update(buy);
                //删除明细数据
                DbBaseClient.Deleteable<w_ReceivableDetail>(p => p.ReceivableCode == buy.ReceivableCode).ExecuteCommand();
                var d = detail.Where(p => !string.IsNullOrEmpty(p.ReceivableDetailCode)).Select(p => p.ReceivableDetailCode).ToList();
                int num = 0;
                d.ForEach(p =>
                {
                    var m = p.Substring(p.Length - 3, 3).ObjToInt();
                    if (num < m)
                    {
                        num = m;
                    }
                });
                detail.ForEach(p =>
                {
                    if (string.IsNullOrEmpty(p.ReceivableCode))
                    {
                        num++;
                        p.ReceivableCode = buy.ReceivableCode;
                        p.ReceivableDetailCode = buy.ReceivableCode + num.ToString("000");
                    }
                });
                //增加明细数据
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }

        public bool DeleteReceivableInfo(w_Receivable buy)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.DeleteById(buy.ID);
                var detail = _sqlSugarClient.Queryable<w_ReceivableDetail>().Where(p => p.ReceivableCode == buy.ReceivableCode).ToList();
                //删除明细数据
                _sqlSugarClient.Deleteable<w_ReceivableDetail>(p => p.ReceivableCode == buy.ReceivableCode).ExecuteCommand();
                decimal? price = 0;
                //修改销售订单数据
                detail.ForEach(p =>
                {
                    var buyInfo = _sqlSugarClient.Queryable<w_Sale>().Where(x => x.SaleCode == p.SaleCode).First();
                    if (buyInfo != null)
                    {
                        buyInfo.TotalPrice1 -= p.Amount4;
                        buyInfo.TotalPrice2 += p.Amount4;
                        price += p.Amount4;
                        _sqlSugarClient.Updateable(buyInfo).ExecuteCommand();
                    }
                });

                //查找结算账户
                var settlementInfo = _sqlSugarClient.Queryable<w_SettlementAccount>()
                    .Where(p => p.Code == buy.SettlementCode).First();
                if (settlementInfo != null)
                {
                    settlementInfo.NowAmount -= price;
                }
                _sqlSugarClient.Updateable(settlementInfo).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }
    }
}