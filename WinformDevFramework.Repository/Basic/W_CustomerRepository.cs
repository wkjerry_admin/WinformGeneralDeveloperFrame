using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_CustomerRepository : BaseRepository<w_Customer>, Iw_CustomerRepository
    {
        public w_CustomerRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}