using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_OtherInWarehouseDetailRepository : BaseRepository<w_OtherInWarehouseDetail>, Iw_OtherInWarehouseDetailRepository
    {
        public w_OtherInWarehouseDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}