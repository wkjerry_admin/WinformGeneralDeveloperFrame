using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_BuyReturnDetailRepository : BaseRepository<w_BuyReturnDetail>, Iw_BuyReturnDetailRepository
    {
        public w_BuyReturnDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}