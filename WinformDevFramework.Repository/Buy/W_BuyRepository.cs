using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar.Extensions;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_BuyRepository : BaseRepository<w_Buy>, Iw_BuyRepository
    {
        private ISqlSugarClient _sqlSugarClient;
        public w_BuyRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {
            _sqlSugarClient=sqlSugar;
        }

        public int AddBuyInfo(w_Buy buy, List<w_BuyDetail> detail)
        {
            int id = 0;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                id=base.Insert(buy);
                int num = 1;
                detail.ForEach(p =>
                {
                    p.BuyID=id;
                    p.BuyCode = buy.BuyCode;
                    p.BuyDetailCode = buy.BuyCode + num.ToString("000");
                    num++;
                });
                var r=_sqlSugarClient.Insertable(detail).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return id;
        }

        public bool UpdateBuyInfo(w_Buy buy, List<w_BuyDetail> detail)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.Update(buy);
                //删除明细数据
                DbBaseClient.Deleteable<w_BuyDetail>(p =>p.BuyID==buy.ID).ExecuteCommand();
                var d = detail.Where(p=>!string.IsNullOrEmpty(p.BuyDetailCode)).Select(p => p.BuyDetailCode).ToList();
                int num = 0;
                d.ForEach(p=>
                {
                    var m = p.Substring(p.Length - 3, 3).ObjToInt();
                    if (num < m)
                    {
                        num = m;
                    }
                });
                detail.ForEach(p =>
                {
                    if (string.IsNullOrEmpty(p.BuyCode))
                    {
                        num++;
                        p.BuyID = buy.ID;
                        p.BuyCode = buy.BuyCode;
                        p.BuyDetailCode = buy.BuyCode + num.ToString("000");
                    }
                });
                //增加明细数据
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }

        public bool DeleteBuyInfo(w_Buy buy)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.DeleteById(buy.ID);
                //删除明细数据
                DbBaseClient.Deleteable<w_BuyDetail>(p => p.BuyID == buy.ID).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }
    }
}