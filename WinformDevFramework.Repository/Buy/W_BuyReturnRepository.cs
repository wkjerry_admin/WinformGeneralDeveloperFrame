using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar.Extensions;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_BuyReturnRepository : BaseRepository<w_BuyReturn>, Iw_BuyReturnRepository
    {
        private ISqlSugarClient _sqlSugarClient;
        public w_BuyReturnRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {
            _sqlSugarClient= sqlSugar;
        }

        /// <summary>
        /// 新增实体 和明细数据
        /// </summary>
        /// <param name="buyReturn"></param>
        /// <param name="detail"></param>
        /// <returns></returns>
        public int AddBuyReturnInfo(w_BuyReturn buyReturn, List<w_BuyReturnDetail> detail)
        {
            int id = 0;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                id = base.Insert(buyReturn);
                int num = 1;
                detail.ForEach(p =>
                {
                    p.BuyReturnCode = buyReturn.BuyReturnCode;
                    p.BuyCode = buyReturn.BuyCode;
                    p.BuyReturnDetailCode = buyReturn.BuyReturnCode + num.ToString("000");
                    num++;
                });
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return id;
        }

        /// <summary>
        /// 修改主表和明细数据
        /// </summary>
        /// <param name="buyReturn"></param>
        /// <param name="detail"></param>
        /// <returns></returns>
        public bool UpdateBuyReturnInfo(w_BuyReturn buyReturn, List<w_BuyReturnDetail> detail)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.Update(buyReturn);
                //删除明细数据
                DbBaseClient.Deleteable<w_BuyReturnDetail>(p => p.BuyReturnCode == buyReturn.BuyReturnCode).ExecuteCommand();
                var d = detail.Where(p => !string.IsNullOrEmpty(p.BuyReturnDetailCode)).Select(p => p.BuyReturnDetailCode).ToList();
                int num = 0;
                d.ForEach(p =>
                {
                    var m = p.Substring(p.Length - 3, 3).ObjToInt();
                    if (num < m)
                    {
                        num = m;
                    }
                });
                detail.ForEach(p =>
                {
                    if (string.IsNullOrEmpty(p.BuyReturnCode))
                    {
                        num++;
                        p.BuyReturnCode = buyReturn.BuyReturnCode;
                        p.BuyCode = buyReturn.BuyCode;
                        p.BuyReturnDetailCode = buyReturn.BuyReturnCode + num.ToString("000");
                    }
                });
                //增加明细数据
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }
    }
}