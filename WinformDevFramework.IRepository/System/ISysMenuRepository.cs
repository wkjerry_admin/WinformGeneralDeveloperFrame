﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.Models;

namespace WinformDevFramework.IRepository.System
{
    public interface ISysMenuRepository:IBaseRepository<sysMenu>
    {

    }
}
