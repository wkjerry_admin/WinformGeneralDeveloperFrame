using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinformDevFramework.IRepository
{
    public interface Iw_PaymentDetailRepository:IBaseRepository<w_PaymentDetail>
    {
    }
}