﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_SaleOutWarehouseDetail")]
    public partial class w_SaleOutWarehouseDetail
    {
           public w_SaleOutWarehouseDetail(){


           }
           /// <summary>
           /// Desc:销售出库单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleOutWarehouseCode {get;set;}

           /// <summary>
           /// Desc:销售出库明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleOutWarehouseDetailCode {get;set;}

           /// <summary>
           /// Desc:销售单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleCode {get;set;}

           /// <summary>
           /// Desc:销售明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleDetailCode {get;set;}

           /// <summary>
           /// Desc:商品编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsName {get;set;}

           /// <summary>
           /// Desc:商品规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsSpec {get;set;}

           /// <summary>
           /// Desc:计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsUnit {get;set;}

           /// <summary>
           /// Desc:仓库编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WarehouseCode {get;set;}

           /// <summary>
           /// Desc:仓库名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WarehouseName {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Number {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
