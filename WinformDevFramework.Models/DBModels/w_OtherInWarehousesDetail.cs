﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_OtherInWarehouseDetail")]
    public partial class w_OtherInWarehouseDetail
    {
           public w_OtherInWarehouseDetail(){


           }
           /// <summary>
           /// Desc:入库单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OtherInWarehouseCode {get;set;}

           /// <summary>
           /// Desc:入库明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OtherInWarehouseDetailCode {get;set;}

           /// <summary>
           /// Desc:仓库编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WarehouseCode {get;set;}

           /// <summary>
           /// Desc:仓库名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WarehouseName {get;set;}

           /// <summary>
           /// Desc:商品编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsName {get;set;}

           /// <summary>
           /// Desc:计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsUnit {get;set;}

           /// <summary>
           /// Desc:商品规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsSpec {get;set;}

           /// <summary>
           /// Desc:入库数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Number {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
