﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_BuyReturnOutWarehouse")]
    public partial class w_BuyReturnOutWarehouse
    {
           public w_BuyReturnOutWarehouse(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:采购退货单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyReturnCode {get;set;}

           /// <summary>
           /// Desc:采购单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyCode {get;set;}

           /// <summary>
           /// Desc:采购退货出库单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyReturnOutWarehouseCode {get;set;}

           /// <summary>
           /// Desc:供应商编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierCode {get;set;}

           /// <summary>
           /// Desc:供应商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierName {get;set;}

           /// <summary>
           /// Desc:单据日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? InvoicesDate {get;set;}

           /// <summary>
           /// Desc:单据附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InvoicesImage {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:单据状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Status {get;set;}

    }
}
