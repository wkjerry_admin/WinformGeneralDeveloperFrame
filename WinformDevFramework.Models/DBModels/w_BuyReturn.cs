﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_BuyReturn")]
    public partial class w_BuyReturn
    {
           public w_BuyReturn(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:退货单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyReturnCode {get;set;}

           /// <summary>
           /// Desc:关联的采购单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyCode {get;set;}

           /// <summary>
           /// Desc:供应商
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierCode {get;set;}

           /// <summary>
           /// Desc:单据日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? InvoicesDate {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MakeUserName {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReviewUserName {get;set;}

           /// <summary>
           /// Desc:单据附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InvoicesImage {get;set;}

           /// <summary>
           /// Desc:单据状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Status {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierName {get;set;}

    }
}
